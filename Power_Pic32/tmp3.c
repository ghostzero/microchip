struct Settings_Data {

		 unsigned char Time_Change_Value;
		 unsigned char AutoStart;
		 unsigned char AutoReplay;
		 unsigned char D_Output_Min;

		 unsigned char A1_Input_Inverse;
		 unsigned char A1_Input_Kp;
		 unsigned char A1_Input_Ki;
		 unsigned char A1_Input_Kd;
		
		 unsigned char A2_Input_Inverse;
		 unsigned char A2_Input_Kp;
		 unsigned char A2_Input_Ki;
		 unsigned char A2_Input_Kd; 

		 unsigned char A3_Input_Inverse;
		 unsigned char A3_Input_Kp;
		 unsigned char A3_Input_Ki;
		 unsigned char A3_Input_Kd;

		 unsigned char D1_P1_Value;
		 unsigned char D2_P2_Value;
		 unsigned char D3_P3_Value;
		 unsigned char D3_P3_Value_X;

		 short int D1_P1_Time;
		 short int D2_P2_Time;
		 short int D3_P3_Time;	

		 short int A1_Input_Calibr_Max;
		 short int A1_Input_Calibr_Min;

		 short int A2_Input_Calibr_Max;
		 short int A2_Input_Calibr_Min;

		 short int A3_Input_Calibr_Max;
		 short int A3_Input_Calibr_Min;
		 	 
        }; 


       struct Settings_Data Eeprom_Settings_Data;