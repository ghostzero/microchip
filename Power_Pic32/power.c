
#include <p32xxxx.h>
#include <sys/clock.h>
#include <math.h>
#include <plib.h>
#include <stdlib.h>

#include <USB/usb.h>
#include <USB/usb_function_generic.h>
#include <USB/usb_function_cdc.h>

#pragma config FPLLMUL = MUL_20, FPLLIDIV = DIV_2, FPLLODIV = DIV_1, FWDTEN = OFF
#pragma config POSCMOD = HS, FNOSC = PRIPLL, FPBDIV = DIV_1, CP = OFF
#pragma config UPLLEN = ON, UPLLIDIV = DIV_2
#pragma config FSOSCEN = OFF  									// 2-�� ��������� ��������� ����� RC13 � RC14 �������� ��� I/O

#pragma config ICESEL = ICS_PGx2 
#pragma config DEBUG = ON


#define Lcd_Command PORTFbits.RF0 
#define Lcd_Enable PORTFbits.RF1

#define	Alarm1_In PORTDbits.RD4
#define	Alarm2_In PORTDbits.RD5
#define	Alarm3_In PORTDbits.RD6
#define	Alarm4_In PORTDbits.RD7

#define Display_Light_On PORTBbits.RB3

void Init( void );
void Display_Init( void );
void Lcd_Enable_Pulse( void );
void Delay( int d );
void Send_Lcd_Command( unsigned char command );
void Send_Lcd_Data( unsigned char data );
void Anfang_Meldung( void );
void Send_Lcd_String( char s[]);
void Set_Cursor( unsigned char Line, unsigned char Position );
void ADC_Conversion( void );
unsigned char Key_Scan(void);
unsigned char Key_Read(void);
char Key_Decode( unsigned char sc_code );
void Recalculate_Pwm_Frequency ( unsigned int freq );
void Frequency_Input( void );
int Input_Number_From_Keyboard ( int, int, int );
void Duty_Cycle_Input(void);
void Recalculate_Voltage_Pwm( void );
void Input_Voltage( void );
void Window2();

void Get_Timer();
void Save_Time(unsigned char hours, unsigned char mins);

void I2C_Delay( int Delay );
void I2C2_Delay( int Delay );

void Milisek_Delay(short int delay );

void Time_Date_Setup_Window();
void Save_Date(unsigned char day, unsigned char month, unsigned char year);

void Scan_Adc_Channels();
void Window3();


// ---------------------------------------------------

void USBCBInitEP(void);

void send_rs(void);

void Command_Processing( void );

void First_Screen( void );

void Keys_Processing( void );


//BOOL USER_USB_CALLBACK_EVENT_HANDLER(int event, void *pdata, WORD size);


// ------------ Global variables ---------------------

int a,Sekunda_Counter, Sekunda, First_Screen_Timer,i,j;
 int Converted_Result2, Converted_Result3, Converted_Result4, Converted_Result8, Converted_Result5, Adc_Timer; 

 struct{
           unsigned First_Start_Flag     :1;
           unsigned First_Screen_Flag    :1;
           unsigned Adc_Convert_Flag     :1;
           unsigned Start_Test_Timer1    :1;
           unsigned Key_Scan_Flag	     :1;
           unsigned Block_Key_Scan_Flag  :1;
           unsigned Block_Usb_Send_Flag  :1;
           unsigned Rs_Data_Ready		 :1;
           unsigned Adc_Data_Ready		 :1;
           unsigned Oben4 :1;
           unsigned Unten4:1;
           unsigned Halt4: 1;
           unsigned Hupe1: 1;
           unsigned Hupe2: 1;
           unsigned Hupe3: 1;
           unsigned Hupe4: 1;
        }Flags;

        char s1[]="F=     Hz"; char s2[]="D=";
	     char s3[]="F2="; char s4[]="D2=";
		  char Clear_Str[5] = "     ", Clear_Str3[] = "   ";
		   char Converted_String_From_Int[7];
			char Converted_String_From_Float[]="       ";
			 char Usb_Send_String[64], Data_String[] = "Hello Serg Aecs-Lab!!! ";
			  char Converted_Int[7];
              

 char Web_Str[] = "www.aecs-lab.com.ua";
  char Info_Str[] = "* - Setup";
	char Dmin_Str[] = "Dmin=", Dmax_Str[] = "Dmax=", Duty_Cycle_Str[]="D=     mks";
	 char PWM_On_Str[] = "PWM ON", PWM_Off_Str[] = "PWM OFF", Clear_Str8[] = "        ";
	  char V_Str[] = "U=     v",  Ur_Str[] = "Ur=     v";

unsigned int AD1CON1_Config, AD1CON2_Config, AD1CON3_Config, AD1CHS_Config, AD1PCFG_Config, AD1CSSL_Config;
 unsigned int Analog_Ports;
   unsigned int Test_Timer1;	

	unsigned int Freq1_Min, Freq2_Min, Freq3_Min, Freq1_Max, Freq2_Max, Freq3_Max, Frequency, Previous_Frequency;
	 unsigned char Frequency_Range;

	  
		int PWM_Frequency, PWM_Duty_Cycle, PWM_Period, Old_Duty_Cycle;
 		  unsigned int PWM_Duty_Cycle_Min, PWM_Duty_Cycle_Max, Duty_Cycle_Percent; 							// ������� PWM_Duty_Cycle � ���������
            double Time_Of_Timer_Increment; 																// ���������� ��� ������� ���������� PWM ������� �� �������
			 double Duty_Cycle;
   
	 	int PWM_Frequency3, PWM_Duty_Cycle3, PWM_Period3, Duty_Cycle3, Old_Duty_Cycle3;  						
			unsigned int PWM_Duty_Cycle_Min3, PWM_Duty_Cycle_Max3, Duty_Cycle_Percent3;


		unsigned int Key_Scan_Timer, Block_Key_Scan_Timer;
		 unsigned char Scan_Code, Pressed_Key;
		  unsigned char PWM_On;

		unsigned int Voltage, Voltage_Min, Voltage_Max;

		unsigned int Usb_Send_Timer, Send_Counter;


USB_HANDLE USBGenericOutHandle; //USB handle.  Must be initialized to 0 at startup.
 USB_HANDLE USBGenericInHandle;  //USB handle.  Must be initialized to 0 at startup.

    unsigned char Expected_Bytes_From_Host, Received_Bytes_Number;
    unsigned char Data_Received_Buffer[64];	  

 struct {

			unsigned char Command;
			unsigned int Frequency;
			unsigned int Freq1_Min;
			unsigned int Freq1_Max;
			float Duty_Cycle;
			unsigned int PWM_Duty_Cycle_Min;
			unsigned int PWM_Duty_Cycle_Max;
			unsigned int Voltage;
			unsigned char PWM_On;

			unsigned char HV_Source_Command_Length;
			unsigned char HV_Source_Command;
			unsigned char HV_Source_In_Data1;
			unsigned char HV_Source_In_Data2;
			unsigned char HV_Source_In_Data3;
			unsigned char HV_Source_In_Data4;
			unsigned char HV_Source_Out_Data1;
			unsigned char HV_Source_Out_Data2;
			unsigned char HV_Source_Out_Data3;
			unsigned char HV_Source_Out_Data4;
			unsigned char HV_Source_Out_Data5;
			unsigned char HV_Source_Out_Data6;
			unsigned char HV_Source_Out_Data7;
			unsigned char HV_Source_Out_Data8;
			unsigned char HV_Source_Out_Data9;
			

   } Rs_Buffer;

  unsigned char Command;
  unsigned int Test_Freq = 0;
  

//  unsigned char OUTPacket[USBGEN_EP_SIZE];

  char char_buff[100];

  unsigned char Uart_Send_Timer, Uart_Send_Flag, Uart_Received_Byte;
  unsigned char Uart_Send_Byte;

  unsigned char Uart_Send_Buffer[5], Uart_Read_Buffer[10], Readed_Byte_Counter, Rx_Flag, Rx_Ready, Rx_Counter;
  unsigned char Rx_Error, Uart_Command_Was_Send_Flag, Read_Bytes_Counter, Number_Of_Bytes_To_Read, Position ;
  unsigned char Wait_Until_Bytes_Not_Read;
  unsigned char Timeout_Timer, Timeout_Flag, Start_Delay_Timer;

  int Display_Light_Timer;

  // --- I2C timer variables -------------------------------------------------- 

  unsigned int Int_Info,I2C_Data_Sekonds, I2C_Data_Minutes, I2C_Data_Hours;
  unsigned char I2C_Flag, I2C_Start_Counter;

  unsigned int Day, Month, Year, Day_Number;

  char Current_Hour, Current_Minutes;

  unsigned char Ack_Flag;

  int Read_Time_Date_Timer;
  unsigned char Time_Date_Ready_Flag, Block_Get_Timer;

  // --------------------------------------------------------------------------

  unsigned int Adc_Data, Adc_Result, Data_To_Send;
  unsigned int Adc_Channel1, Adc_Channel2, Adc_Channel3, Adc_Channel4;

 // ---------------------------------------------------------------------------

 short int Msk_Delay; 
 unsigned char Window3_Active;

 unsigned char  Alarm4_Flag;
 unsigned int Alarm4_Timer;

 unsigned char PWM_Was_On;


 
int main(void){  
 
   Sekunda_Counter = 0; Sekunda = 0; Msk_Delay = 0;
   a = 0; Window3_Active = 0;

	Freq1_Min = 100 ; Freq1_Max = 1000;				// ��������� ���� � ������
	 //Freq2_Min = 1000 ; Freq2_Max = 10000;
	 // Freq3_Min = 10000 ; Freq3_Max = 100000;
	
	Frequency = Freq1_Min; Frequency_Range = 1; Previous_Frequency = Frequency;

			  Old_Duty_Cycle = 0;
			  	PWM_Duty_Cycle_Min3 = 0; PWM_Duty_Cycle_Max3 = 100; Duty_Cycle3 = 0; Old_Duty_Cycle = 0;


	PWM_Duty_Cycle_Min = 1; PWM_Duty_Cycle_Max = 100;		// � �������������, �.�. 100 ������ ��� �����������. �� PWM_Duty_Cycle_Max ����� ����������� ��� ��������� �������
	 Duty_Cycle =  PWM_Duty_Cycle_Min;						// ���������� Duty_Cycle ��� ����������� PWM_Duty_Cycle �� ������� � ������������� 
			

 		    First_Screen_Timer = 0; Flags.First_Screen_Flag = 0;
 			 Flags.First_Start_Flag = 0;	
			
			Key_Scan_Timer = 0; Block_Key_Scan_Timer = 0;
			 Flags.Key_Scan_Flag = 0; Flags.Block_Key_Scan_Flag = 0;

 			Scan_Code = 7; PWM_On = 0;

			Voltage_Min = 10;  Voltage_Max = 1000; Voltage = Voltage_Min;

   USBGenericOutHandle = 0;	
	USBGenericInHandle = 0;

   Read_Time_Date_Timer = 0;
   Time_Date_Ready_Flag = 0;	
	
			Init();  

			  Start_Delay_Timer = 0;
				while(1){
	        			if ( Start_Delay_Timer >= 100 ) break;
						}
 
    		 Display_Init();

			USBDeviceInit();		
			 USBDeviceAttach();
								
		//	PORTDbits.RD1 = 0; 	PORTGbits.RG6 = 0;  

			Flags.Block_Usb_Send_Flag = 0;  Usb_Send_Timer = 0;	Send_Counter = 0;
			
			Command = 0;
 			Flags.Rs_Data_Ready = 0;
			Rs_Buffer.PWM_On = 0;
			PWM_On = 0;

	Uart_Send_Timer = 0;  Uart_Send_Flag = 0;  Uart_Received_Byte = 0;
	Uart_Send_Byte = 0;

  for( i = 0; i < 5; i++ ) Uart_Send_Buffer[i] = 0;
  for( i = 0; i < 10; i++ ) Uart_Read_Buffer[i] = 0;

  Readed_Byte_Counter = 0; Rx_Flag = 0; Rx_Ready = 0; Rx_Counter;
  Rx_Error = 0;

  Uart_Command_Was_Send_Flag = 0; Read_Bytes_Counter = 0, Number_Of_Bytes_To_Read = 0; 

  Wait_Until_Bytes_Not_Read = 0; Timeout_Timer = 0; Timeout_Flag = 0;
    
  Display_Light_Timer = 0;  
  Block_Get_Timer = 0;    
             	     
  Frequency_Range = 1; 	Alarm4_Timer = 0; Alarm4_Flag = 0; PWM_Was_On = 0;

while(1){
	   
		if( Rx_Ready == 1 ){
							Rx_Ready = 0;	
							}
																		
 		send_rs();
		 Command_Processing();
		  First_Screen(); 							
	       Keys_Processing();

	    if( Flags.Adc_Convert_Flag == 1 ){
									  	   Scan_Adc_Channels();
									        Flags.Adc_Convert_Flag = 0;
									         Adc_Timer = 0; 
									          Flags.Adc_Data_Ready = 1;
									     }
				
			}	// end of while
  
  return(0);
   
}						// end of main

//------------------ TIMER1 Interrupt Vector --- ( Various Time Counters )------------------------


void __ISR( _TIMER_1_VECTOR , ipl7)Timer1Handler(void)					// ���������� ������� 1 ������������ ��� ������� ��������� ��������� ����������
{ 
	Sekunda_Counter++; 
												    // ���� Counter �������� �� 100, �� ��� 1 �������	
	if( Sekunda_Counter == 100 ){ 
								 Sekunda_Counter = 0; Sekunda++;  PORTD = PORTD ^ 0x02; 
								  First_Screen_Timer++;	
								}	

    Adc_Timer++;       												    // ������ ����� ��������� �������� ����������� ��� �� �������������� 
  		if( Adc_Timer == 50){ Adc_Timer = 0; Flags.Adc_Convert_Flag = 1; }

	Key_Scan_Timer++;	
	    if( Key_Scan_Timer == 10 ){ Flags.Key_Scan_Flag = 1; Key_Scan_Timer = 0; } 

	Block_Key_Scan_Timer++;
	
        if ( Block_Key_Scan_Timer == 30 ){ Block_Key_Scan_Timer = 0 ; Flags.Block_Key_Scan_Flag = 0; } 

	Usb_Send_Timer++;

		if( Usb_Send_Timer == 100 ) { Usb_Send_Timer = 0; Flags.Block_Usb_Send_Flag = 0;}	

	Uart_Send_Timer++;

	    if( Uart_Send_Timer == 100 ){ Uart_Send_Timer = 0; Uart_Send_Flag = 1; }


	if( Timeout_Flag == 1 ){ Timeout_Timer++;

						 if( Timeout_Timer == 200 ){ 
													Timeout_Timer = 0; Timeout_Flag = 0, Wait_Until_Bytes_Not_Read = 0;
													 Number_Of_Bytes_To_Read = 0;
							          				  Read_Bytes_Counter = 0; 
								   					}
							}	

    Start_Delay_Timer++;
    				  


IFS0bits.T1IF = 0;

}

//------------------ TIMER2 Interrupt Vector --- ( PWM2 - ��� ��� ������ )-----------------------


void __ISR( _TIMER_2_VECTOR , ipl5)Timer2Handler(void)					// Timer2 works for PWM2 Module
{ 

int a = 0;
 a++;
 a++;
 a++;
//PORTG^=64;
IFS0bits.T2IF = 0;

}

//------------------ TIMER3 Interrupt Vector --- ( PWM3 )---------------------------------------------


void __ISR( _TIMER_3_VECTOR , ipl4)Timer3Handler(void)				
{ 

int a = 0;
 a++;
 a++;
 a++;
//PORTG ^= 64;
IFS0bits.T3IF = 0;

}

//------------------ TIMER5 Interrupt Vector --- ( Various Time Counters by means of Timer4 + Timer5 )-------


void __ISR( _TIMER_5_VECTOR , ipl6)Timer5Handler(void)			 
{														// ���������� ����������� � ���������� 1 �����������					 

	Display_Light_Timer++;  
	if( Display_Light_Timer == 30000) Display_Light_On = 0;  // ����� 30 ������ ��������� ��������� ������� 

		IFS0bits.T5IF = 0;

	Read_Time_Date_Timer++;
    if( Read_Time_Date_Timer == 500 ){ 
									  Read_Time_Date_Timer = 0; 
									   if( Block_Get_Timer == 0 )Get_Timer();
									     Time_Date_Ready_Flag = 1; 
										} 


	if( Alarm4_Flag == 1 ) Alarm4_Timer++;
	if( Alarm4_Timer >= 200 ){
							  Alarm4_Timer = 0;
							  Alarm4_Flag = 0;
							  if( (PWM_On == 1) && ( Alarm4_In == 1 ) ){
																		TMR2 = 0; TMR3 = 0;
																		a++; a++;
																	    T2CONbits.ON = 1; 
						        										T3CONbits.ON = 1;
																		a++;
							    										a++; 
																		OC2CON = OC2CON | 0x00008000;
																		OC1CON = OC1CON | 0x00008000;
							  											}
							  }
						 	
    if( Alarm4_Flag == 0 )
	       if( Alarm4_In == 0 ){
						        Alarm4_Flag = 1;
						        Alarm4_Timer = 0;
								T2CONbits.ON = 0; 
						        T3CONbits.ON = 0;
							    a++;  
						        OC2CON = OC2CON & ~ 0x00008000;
								OC1CON = OC1CON & ~ 0x00008000;
								TMR2 = 0; TMR3 = 0;
						       }				 
												

	Msk_Delay++;

 
  

 
}

// ------------ UART2 Interrupt Vector ------------------------------------------------------------------------------


void __ISR( _UART_2_VECTOR, ipl3 ) UART2Handler(void)				
{ 
	
    Rx_Flag++;

	Uart_Received_Byte = U2RXREG;

	if( Uart_Command_Was_Send_Flag == 1 ){ Read_Bytes_Counter = 0; Uart_Command_Was_Send_Flag = 0; Number_Of_Bytes_To_Read = Uart_Received_Byte; }

	Uart_Read_Buffer[Read_Bytes_Counter] = Uart_Received_Byte;
	Read_Bytes_Counter++;

	if( Read_Bytes_Counter == (Number_Of_Bytes_To_Read + 1 ) ){ 
															   Rx_Ready = 1;
															   Wait_Until_Bytes_Not_Read = 0; 
															   Timeout_Timer = 0;
															   Timeout_Flag = 0;
															   Read_Bytes_Counter = 0;
																 }

 

    if( (U2STAbits.PERR == 1 ) || ( U2STAbits.FERR == 1 ) || ( U2STAbits.OERR == 1 ) ) Rx_Error = 10;
 
 	IFS1bits.U2RXIF = 0;
	IFS1bits.U2EIF = 0;

}

// ------------SPI2 Interrupt Vector -----------------------------------------------------------------------------

void __ISR( _SPI_2_VECTOR, ipl3 ) SPI2Handler(void)				
{ 

/*
 if( IFS1bits.SPI2TXIF == 1 ){  Spi_Data_TX = 1; }
 if( IFS1bits.SPI2RXIF == 1 ){ Spi_Data_RX = 1; }
 if( IFS1bits.SPI2EIF == 1 ){  Spi_Data_ER = 1; }
					*/

 IFS1bits.SPI2EIF = 0;
 IFS1bits.SPI2RXIF = 0;
 IFS1bits.SPI2TXIF = 0;
  
}

// -------- I2C1 Interrupt Vector ---------------------------------------------------------------------------------

void __ISR( _I2C_1_VECTOR, ipl2 ) I2C1Handler(void)				
{ 

 IFS0bits.I2C1BIF = 0;
 IFS0bits.I2C1SIF = 0;
 IFS0bits.I2C1MIF = 0;;

}

// ---------- Init function -----------------------------------------------------------------------------------------

void Init( void ){

  TRISE = 0;
   TRISF = 0; 
	TRISB = 0x1500;           // RB10, RB8 and RB12 - Digital Input.  �������� ����������	
	 DDPCON = 0;            // ��������� JTAG ������� �� ��������� ������� � ������������ � ������ RB10, RB11 � RB12 � �� ���� �� ���� ���������
	  TRISD = 0;

	TRISDbits.TRISD4 = 1;
	TRISDbits.TRISD5 = 1;
	TRISDbits.TRISD6 = 1;
	TRISDbits.TRISD7 = 1;

	   PORTD = 0;

	   PORTDbits.RD9 = 1;
	   PORTDbits.RD10 = 1;

	Display_Light_On = 0;  //

// -------------- Timer1 settings --------------------------------------------------------

T1CON = 0; a++;
  TMR1 = 0; 
	PR1 = 6250;
	
   T1CONbits.TCKPS1 = 1;
	 T1CONbits.TCKPS0 = 1;
		T1CONbits.TGATE = 0;
		    T1CONbits.TCS = 0;
				a++; a++;
       				T1CONbits.ON = 1;

// -- Timer1 interrupts enabling ---

 INTEnable(INT_T1, INT_ENABLED);
  INTSetVectorPriority(INT_TIMER_1_VECTOR, INT_PRIORITY_LEVEL_7);
    INTSetVectorSubPriority(INT_TIMER_1_VECTOR, INT_SUB_PRIORITY_LEVEL_3);
  	  INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR); 						// Enable multi-vector interrupts


// -------------- Timer2 ON --- Timer2 works for PWM2 Module - ��� ��� ������ ------------

  unsigned int Timer2_Period;

     Timer2_Period = 0xFFFF;  // 0x7EF5;
		OpenTimer2( T2_PS_1_16 | T2_32BIT_MODE_OFF , Timer2_Period ); // 1 ��������� �������2 = 0.2 ������������
		//T2CONbits.ON = 0; 

// -- Timer2 interrupts enabling --

  INTSetVectorPriority(INT_TIMER_2_VECTOR, INT_PRIORITY_LEVEL_5);
    INTSetVectorSubPriority(INT_TIMER_2_VECTOR, INT_SUB_PRIORITY_LEVEL_3);
	 // INTEnable(INT_T2, INT_ENABLED);	  	  

// --------------- Timer3 ON --- Timer3 works for PWM1 Module ---------------------------

	OpenTimer3( T3_PS_1_1 , 0xFFFF );

// -- Timer3 interrupts enabling --

  INTSetVectorPriority(INT_TIMER_3_VECTOR, INT_PRIORITY_LEVEL_4);
    INTSetVectorSubPriority(INT_TIMER_3_VECTOR, INT_SUB_PRIORITY_LEVEL_3);
	  //INTEnable(INT_T3, INT_ENABLED); 	  


// -------------- Timer4 ON in 32-bit Mode ---- Timer4 + Timer5 -------------------------

  unsigned int Timer4_Period;

     Timer4_Period = 0x13880;  // 0x13880 => 80000							   
	  OpenTimer4(T4_ON | T4_PS_1_1 | T4_32BIT_MODE_ON | T4_SOURCE_INT, Timer4_Period ); // ������ ������ ������������ ���������� ������� 1 �����������

// -- Timer5 interrupts enabling --

  INTSetVectorPriority(INT_TIMER_5_VECTOR, INT_PRIORITY_LEVEL_6);
    INTSetVectorSubPriority(INT_TIMER_5_VECTOR, INT_SUB_PRIORITY_LEVEL_3);
	  INTEnable(INT_T5, INT_ENABLED);	  	  
  	 	INTEnableInterrupts();


// ------ PWM2 configuration and start up -----------------------------------------------

													// 1 ��������� �������2 = 0.2 ������������ � ��� ��������� ����� ������� �� 5 �������� 1 ������������
 													// � ������� ��� ���������� PWM_Duty_Cycle ������ ����� ������������ 
	PWM_Frequency = Frequency; 
 	 Time_Of_Timer_Increment = (1.0/80*0.000001)*16;  								// 16 - Timer2 Prescaller Value. 0.2 mkS - ����� ���������� �������2 �� 1
 	  PWM_Period = (int)( ( 1.0/(double)PWM_Frequency ) / Time_Of_Timer_Increment );
	   PWM_Duty_Cycle =  PWM_Duty_Cycle_Min * 5 ;   								// PWM_Duty_Cycle �������� � �������������. �������� �� 5, ����� ��� PWM_Duty_Cycle ��� 1 ������������
		 PWM_Duty_Cycle_Max = ( PWM_Period / 2.0 ) * 0.2;								// �������� �� 0.2 mkS, ��������� PWM_Duty_Cycle_Max �� ����� ������� � ������������ 
			a++;

	 T2CONbits.ON = 0;  															// ����� ������� �������� � PR2 ���� ������������� ������
	 	a++;
		 PR2 = PWM_Period;		   													// ������������� ����������� �������� ������� ���
		   a++;
			// OpenOC2(OC_ON | OC_TIMER_MODE16 | OC_TIMER2_SRC | OC_PWM_FAULT_PIN_DISABLE, PWM_Duty_Cycle,PWM_Duty_Cycle);
		    //
	OpenOC2( OC_TIMER_MODE16 | OC_TIMER2_SRC | OC_PWM_FAULT_PIN_DISABLE, PWM_Duty_Cycle,PWM_Duty_Cycle);
			 
			  // T2CONbits.ON = 1;  // ��� � ������ ���������� ����� �������� ����� �� ���������� ��� � �����
 

// ------ PWM3 configuration and start up -----------------------------------------------


 	
	PWM_Frequency3 = 50000;  			// 50 kHz 
 	  Time_Of_Timer_Increment = (1.0/80*0.000001)*1;  									// 1 - Timer3 Prescaller Value
 	    PWM_Period3 = (int)( ( 1.0/(double)PWM_Frequency3 ) / Time_Of_Timer_Increment );
			PWM_Duty_Cycle3 = (int) ( PWM_Period3 / 2 );	
				a++;

	 T3CONbits.ON = 0;  															// ����� ������� �������� � PR2 ���� ������������� ������
	 	a++;
		 PR3 = PWM_Period3;														   // ������������� ����������� �������� ������� ���
		   a++;
			 OpenOC1( OC_TIMER_MODE16 | OC_TIMER3_SRC | OC_PWM_FAULT_PIN_DISABLE, PWM_Duty_Cycle3,PWM_Duty_Cycle3 );
			    T3CONbits.ON = 0;
			

// ----------------- ADC Init-------------------------------------------------------------

/*

AD1CON1_Config = ADC_MODULE_ON | ADC_IDLE_STOP | ADC_FORMAT_INTG | ADC_CLK_MANUAL  
																  | ADC_AUTO_SAMPLING_OFF | ADC_SAMP_OFF ;

AD1CON2_Config = ADC_VREF_AVDD_AVSS | ADC_OFFSET_CAL_DISABLE | ADC_SCAN_OFF 
															   | ADC_SAMPLES_PER_INT_1 | ADC_BUF_16 ; 

AD1CON3_Config = ADC_CONV_CLK_PB | ADC_SAMPLE_TIME_1 | ADC_CONV_CLK_Tcy ;															
																	 
Analog_Ports = ENABLE_AN2_ANA ;

						*/
   AD1PCFG = 0xffff; 

/*

   SetChanADC10( ADC_CH0_POS_SAMPLEA_AN2 | ADC_CH0_NEG_SAMPLEA_NVREF );

     OpenADC10( AD1CON1_Config, AD1CON2_Config, AD1CON3_Config, ENABLE_AN2_ANA | ENABLE_AN3_ANA | ENABLE_AN4_ANA
																| ENABLE_AN8_ANA, SKIP_SCAN_ALL); 
					*/

  // AD1PCFG = 0xffff ^ ENABLE_AN1_ANA ;
 
	//	Adc_Timer = 0; Flags.Adc_Convert_Flag = 0;

		//PORTB = 0x1E00;

	a++;
     
     
// ----------------- ��������� UART2 ---------------------------------------------------------------------------

  //U2MODEbits.ON = 1;
  U2MODEbits.UEN1   =  0 ;
  U2MODEbits.UEN0   =  0 ;
  U2MODEbits.PDSEL1 =  0 ;
  U2MODEbits.PDSEL0 =  0 ;
  U2MODEbits.STSEL  =  1 ;

  U2STAbits.URXISEL1 = 0;
  U2STAbits.URXISEL0 = 0;

  U2BRG = 2082;

  U2STAbits.UTXEN = 1;
  for( j = 0; j < 10; j++);

  U2STAbits.URXEN = 1;
  for( j = 0; j < 10; j++);

  U2MODEbits.ON = 1;
  for( j = 0; j < 10; j++);

  INTSetVectorPriority( INT_UART_2_VECTOR, INT_PRIORITY_LEVEL_3 );
   INTSetVectorSubPriority( INT_UART_2_VECTOR, INT_SUB_PRIORITY_LEVEL_3 );
	INTEnable( INT_U2RX , INT_ENABLED );


// ----------------- I2C1 Configuration ------------------------------------------------------------------------


	INTSetVectorPriority( INT_I2C_1_VECTOR, INT_PRIORITY_LEVEL_2 );
     INTSetVectorSubPriority( INT_I2C_1_VECTOR, INT_SUB_PRIORITY_LEVEL_3 );
	  INTEnable( INT_I2C1, INT_ENABLED );

		I2CConfigure ( I2C1, I2C_STOP_IN_IDLE |I2C_ENABLE_HIGH_SPEED );
		I2CSetSlaveAddress(I2C1, 0x0F, 0, I2C_USE_7BIT_ADDRESS );
	    I2C1BRG = 89;    			// 400 kHz
		I2C1CONbits.ACKDT = 0;   	// ����� ������ Acknowledge 
		I2CEnable(I2C1, TRUE);

		I2C1CONbits.RCEN = 0;

		I2C1CONbits.PEN = 1;
 		while( I2C1CONbits.PEN == 1){}

		I2C1CONbits.PEN = 1;
 		while( I2C1CONbits.PEN == 1){}

		I2C1CONbits.PEN = 1;
 		while( I2C1CONbits.PEN == 1){}
							

// ----------------- I2C2 Configuration --------------------------------------------------------------------------
	
		I2CConfigure ( I2C2, I2C_STOP_IN_IDLE | I2C_ENABLE_HIGH_SPEED );
		I2CSetSlaveAddress(I2C2, 0x0F, 0, I2C_USE_7BIT_ADDRESS );
	    I2C2BRG = 89;    			// 400 kHz
		I2C2CONbits.ACKDT = 0;   	// ����� ������ Acknowledge 
		I2CEnable(I2C2, TRUE);

		I2C2CONbits.RCEN = 0;

		I2C2CONbits.PEN = 1;
 		while( I2C2CONbits.PEN == 1){}

// --------------- SPI Init -----------------------------------------------------------------------------------


   TRISGbits.TRISG9 = 0;   	    // slave select

	TRISGbits.TRISG6 = 0;       // SCK
	 TRISGbits.TRISG7 = 1;      // SDI
	   TRISGbits.TRISG8 = 0;    // SDO
	
     PORTGbits.RG9 = 1;

	// PORTGbits.RG7 = 0;

   SpiChnOpen(SPI_CHANNEL2 , SPI_OPEN_MSTEN | SPI_OPEN_MODE32 | SPI_OPEN_CKP_HIGH  , 511 );

   SPI2BRG = 255;   		 //  SPI SCK  ������� 156.25 ���

    INTSetVectorPriority( INT_SPI_2_VECTOR, INT_PRIORITY_LEVEL_3 );
     INTSetVectorSubPriority( INT_SPI_2_VECTOR, INT_SUB_PRIORITY_LEVEL_2 );
	  INTEnable( INT_SPI2, INT_ENABLED );


// -----------------------------------------------------------------------------------------------------------	

}  // end of Init

// ------------------ Milisek Delay Subroutine ----------------------------------------------------------------

void Milisek_Delay( short int delay ){
									
						 Msk_Delay = 0;

				 		 while( Msk_Delay < delay ) {}


}

// --------------------------- LCD Winstar Initialization -----------------------------------------------------

void Display_Init(void){

  TRISE = 0;

  Lcd_Command = 0;
  Lcd_Enable = 0;


  PORTE = 0x3F;  
   Lcd_Command = 0;		 	
     Lcd_Enable_Pulse();
	  Delay(5800);
 		PORTE = 0x0F;
		 Lcd_Enable_Pulse();
	  	   Delay(5800);
	 		PORTE = 0x01;
			 Lcd_Enable_Pulse();
	  		   Delay(5800);
 				
			
   //Send_Lcd_Data(67);

   		Anfang_Meldung();                             


}

void Lcd_Enable_Pulse(void){

  int i;
   Lcd_Enable = 1 ;
    for( i = 0 ; i <= 20 ; i++ )
	  Lcd_Enable = 0;
}


void Delay( int d ){ 
  	   
	   int i;
    	for( i = 0; i < d; i++);
}


void Send_Lcd_Command(unsigned char command ){

			PORTE = command;  
  			 Lcd_Command = 0;		 	// RC14
     		   Lcd_Enable_Pulse();
				if(command == 0x01)Delay(1800);
	  			 else Delay(100);
}

void Send_Lcd_Data(unsigned char data ){

			PORTE = data;  
  			 Lcd_Command = 1;		 	// RC14
     		   Lcd_Enable_Pulse();
	  			 Delay(50);
}

void Anfang_Meldung( void ){

	unsigned char index, c;
		char str1[] = "Power V1.0";
		char str2[] = "Press any key";
     
 index = 0;
    while ( str1[index]!='\0' ){                      // �������� �� 1-�� ������ ������ ���������
                        c = str1[index];
                        Send_Lcd_Data(c);
                        index++;                          
                             } // end of while

Send_Lcd_Command(0xC0);

 index = 0;
    while ( str2[index]!='\0' ){                      // �������� �� 1-�� ������ ������ ���������
                        c = str2[index];
                        Send_Lcd_Data(c);
                        index++;                          
                             } // end of while

	Set_Cursor(4,1);
	 Send_Lcd_String( Web_Str );			

}

void Send_Lcd_String( char s[] ){
 
					unsigned char index;
						index = 0;

    		while ( s[index]!='\0' ){                     
                             Send_Lcd_Data(s[index]);
                        		index++;                          
                             } 
}

void Set_Cursor( unsigned char Line, unsigned char Position ){

		    unsigned char Cmd;
		
		if( Line == 1 ) Cmd = 0x80 + Position -1 ;
			if( Line == 2 ) Cmd = 0xC0 + Position - 1;
				if( Line == 3 ) Cmd = 0x94 + Position - 1;
					if( Line == 4 ) Cmd = 0xD4 + Position - 1;
				
						Send_Lcd_Command(Cmd);

}

// ----------------------------------------------------------------------------------------------------------


unsigned char  Key_Scan( void ){

     unsigned char sc_code;

	 unsigned char full_scan_code1, full_scan_code2, full_scan_code3, full_scan_code4;    
									
       PORTBbits.RB13  = 0;				// RB9 - RB12 - ������ ���������, a RB14, RD5 and RD6 - ������� 
	   PORTBbits.RB11  = 1;
	   PORTBbits.RB9   = 1;
	   PORTBbits.RB14  = 1;		     
       	a++;
        a++;
        a++;
        a++;
     		sc_code = Key_Read();   
       			//if(  sc_code != 0x07) return( sc_code |  ( (PORTB & 0x2000) >> 10 ) | ( (PORTB & 0x0800) >> 7 ) |
				//																( (PORTB & 0x0200) >> 4 ) | ( (PORTB & 0x4000) >> 8 ) );

	if(  sc_code != 0x07){
            full_scan_code1 = ( sc_code |  ( (PORTB & 0x2000) >> 10 ) | ( (PORTB & 0x0800) >> 7 ) |	
																				( (PORTB & 0x0200) >> 4 ) | ( (PORTB & 0x4000) >> 8 ) );
						}
		else full_scan_code1 = 0;					 

 
       PORTBbits.RB13  = 1;
	   PORTBbits.RB11  = 0;
	   PORTBbits.RB9   = 1;
	   PORTBbits.RB14  = 1;		      
        a++;
        a++;
        a++;
        a++;
     		sc_code = Key_Read();   
       			//if(  sc_code != 0x07) return( sc_code |  ( (PORTB & 0x2000) >> 10 ) | ( (PORTB & 0x0800) >> 7 ) |
				//																( (PORTB & 0x0200) >> 4 ) | ( (PORTB & 0x4000) >> 8 ) );

	if(  sc_code != 0x07){
			full_scan_code2 = ( sc_code |  ( (PORTB & 0x2000) >> 10 ) | ( (PORTB & 0x0800) >> 7 ) |
																				( (PORTB & 0x0200) >> 4 ) | ( (PORTB & 0x4000) >> 8 ) ) ;
						}
		else full_scan_code2 = 0;					

       PORTBbits.RB13  = 1;
	   PORTBbits.RB11  = 1;
	   PORTBbits.RB9   = 0;
	   PORTBbits.RB14  = 1;		      
        a++;
        a++;
        a++;
        a++;
			sc_code = Key_Read();   
       			//if(  sc_code != 0x07) return( sc_code |  ( (PORTB & 0x2000) >> 10 ) | ( (PORTB & 0x0800) >> 7 ) |
				//																( (PORTB & 0x0200) >> 4 ) | ( (PORTB & 0x4000) >> 8 ) );

	if(  sc_code != 0x07){
			full_scan_code3 = ( sc_code |  ( (PORTB & 0x2000) >> 10 ) | ( (PORTB & 0x0800) >> 7 ) |
																				( (PORTB & 0x0200) >> 4 ) | ( (PORTB & 0x4000) >> 8 ) ); 
					     }
		else full_scan_code3 = 0;					 

 
       PORTBbits.RB13  = 1;
	   PORTBbits.RB11  = 1;
	   PORTBbits.RB9   = 1;
	   PORTBbits.RB14  = 0;		     
        a++;
        a++;
        a++;
        a++;
    		sc_code = Key_Read();   
       			//if(  sc_code != 0x07) return( sc_code |  ( (PORTB & 0x2000) >> 10 ) | ( (PORTB & 0x0800) >> 7 ) |
				//																( (PORTB & 0x0200) >> 4 ) | ( (PORTB & 0x4000) >> 8 ) );
															  
    if(  sc_code != 0x07){
			full_scan_code4 =  ( sc_code |  ( (PORTB & 0x2000) >> 10 ) | ( (PORTB & 0x0800) >> 7 ) |
																				( (PORTB & 0x0200) >> 4 ) | ( (PORTB & 0x4000) >> 8 ) );
						  }
		else full_scan_code4 = 0;		  


    if( (full_scan_code1 == 0) && (full_scan_code2 == 0) && (full_scan_code3 == 0) && (full_scan_code4 == 0) ) 
     																										return( 7 );
	 else return( full_scan_code1 ^ full_scan_code2 ^ full_scan_code3 ^ full_scan_code4 );
       
}

// --------------------------------------------------------------------------------

unsigned char Key_Read(void){

		return( ( (PORTB & 0x0400) >> 10) | ( ( PORTB & 0x0100) >> 7 ) | ( (PORTB & 0x1000) >> 10 ) );

}

// --------------------------------------------------------------------------------

 char Key_Decode(unsigned char sc_code){

   if ( sc_code == 62 ) return('1');
   if ( sc_code == 61 ) return('2');
   if ( sc_code == 59 ) return('3');
   if ( sc_code == 94 ) return('4');
   if ( sc_code == 93 ) return('5');
   if ( sc_code == 91 ) return('6');
   if ( sc_code == 110 ) return('7');
   if ( sc_code == 109 ) return('8');
   if ( sc_code == 107 ) return('9');
   if ( sc_code == 118 ) return('*');
   if ( sc_code == 117 ) return('0');
   if ( sc_code == 115 ) return('#');

   if ( sc_code == 72 ) return(11);   // ������� ������ * � 1
   if ( sc_code == 75 ) return(12);   // ������� ������ * � 2
   if ( sc_code == 77 ) return(13);   // ������� ������ * � 3

     return(' ');
}

// -------------------------------------------------------------------------------

void Recalculate_Pwm_Frequency ( unsigned int freq ){
													  // ������ PWM_Period ��� ��������� �������
 unsigned char Prescale_Value;
			
 Prescale_Value = 16;

 PWM_Frequency = freq;
 
  if( Frequency_Range == 1 ) Prescale_Value = 16;
  if( Frequency_Range == 2 ) Prescale_Value = 8;
  if( Frequency_Range == 3 ) Prescale_Value = 1;

 	 Time_Of_Timer_Increment = ((double)1.0/80.0*0.000001) * (double)Prescale_Value;  									
 	  PWM_Period = (int)( ( (double)1.0/(double)PWM_Frequency ) / Time_Of_Timer_Increment );   // ������ ���� � ����� �������

       T2CONbits.ON = 0;
  														
	 	a++;
		 PR2 = PWM_Period;		   													
		   a++;


    if( Frequency_Range == 1 ){
						       T2CONbits.TCKPS0 = 0;
			 				    T2CONbits.TCKPS1 = 0;
			  				     T2CONbits.TCKPS2 = 1;
							   }							
 	
	if( Frequency_Range == 2 ){
						       T2CONbits.TCKPS0 = 1;
			 				    T2CONbits.TCKPS1 = 1;
			  				     T2CONbits.TCKPS2 = 0;
							   }	

	if( Frequency_Range == 3 ){
						       T2CONbits.TCKPS0 = 0;
			 				    T2CONbits.TCKPS1 = 0;
			  				     T2CONbits.TCKPS2 = 0;
							   }											
 			 
	//	T2CONbits.ON = 1;
		
  if( Frequency_Range == 1 ) PWM_Duty_Cycle_Max = (int)(( PWM_Period / 2.0 ) * 0.2 );
   if( Frequency_Range == 2 ) PWM_Duty_Cycle_Max = (int)(( PWM_Period / 2.0 ) * 0.1 );
	 if( Frequency_Range == 3 ) PWM_Duty_Cycle_Max = (int)(( PWM_Period / 2.0 ) * 0.0125 );

	
											// ���� ��� ��������� ������� Duty_Cycle ���� ������ PWM_Duty_Cycle_Max, �� ������������ Duty_Cycle � PWM_Duty_Cycle
			if( Duty_Cycle > (double)PWM_Duty_Cycle_Max ){   
												   Duty_Cycle = (double)PWM_Duty_Cycle_Max;

													if( Frequency_Range == 1 )PWM_Duty_Cycle = (int)(Duty_Cycle * 5);
													 else if( Frequency_Range == 2 )PWM_Duty_Cycle = (int)(Duty_Cycle * 10);
													  else if( Frequency_Range == 3 )PWM_Duty_Cycle = (int)(Duty_Cycle * 80);
  
													 OC2RS =  PWM_Duty_Cycle;			   // OC2RS - ������� ��� PWM_Duty_Cycle	

												if( !Window3_Active ){
													  				  Set_Cursor(1,13);
													   				  sprintf( Converted_String_From_Float, "%7.1f", Duty_Cycle );
													   				  Send_Lcd_String( Clear_Str );
													     			  Set_Cursor(1,13);
														  			  Send_Lcd_String( Converted_String_From_Float );
																	 }					 
												  }
	if( PWM_On == 1 ) T2CONbits.ON = 1;
	
if( !Window3_Active ){
					   Set_Cursor(1,3);
	 				    sprintf( Converted_String_From_Int, "%5d", Frequency );
	  					 Send_Lcd_String( Clear_Str );
	   					  Set_Cursor(1,3);
						   Send_Lcd_String( Converted_String_From_Int );
		 					Set_Cursor(2,16);	
		  					 sprintf( Converted_String_From_Int, "%5d", PWM_Duty_Cycle_Max );
		   					  Send_Lcd_String( Clear_Str );
							   Set_Cursor(2,16);
			 					Send_Lcd_String( Converted_String_From_Int );
					 }								  								
			
}

// ----------------------------------------------------------------------------------------------------------------------------------------------

void Frequency_Input( void ){

			int Number, Total_Digits;

			Set_Cursor(4,1);
			 Send_Lcd_String(s1);					// ������� � 4-�� ������ "F=     Hz" � ������ � ���������� �������      
			   Set_Cursor(4,3);

			 Total_Digits = 0; Number = 0;

              Block_Key_Scan_Timer = 0; Flags.Block_Key_Scan_Flag = 1; 

  while( (Total_Digits < 6) &&  ( Pressed_Key != '*' ) && ( Pressed_Key != '#' )  ) {

		if( ((Scan_Code = Key_Scan()) != 7) && (Flags.Block_Key_Scan_Flag != 1) ){

											Block_Key_Scan_Timer = 0; Flags.Block_Key_Scan_Flag = 1;  // ������ � ���� ��� ���������� ��������
										    Pressed_Key = Key_Decode( Scan_Code );
										     if( (Pressed_Key != '*' ) && ( Pressed_Key != '#' ) ){
																								   Number = Number * 10 + (Pressed_Key - 48 );
																								   Total_Digits++;	
																								   Send_Lcd_Data( Pressed_Key );
																									}																	 									  													

																					}   // end of if( ((Scan_Code = Key_Scan()) .....									
												 	
																					  }	// end of while( (Total_Digits < 5) .........	

	 if( (Pressed_Key == '#' ) || (Total_Digits == 6) ){  

							  Frequency = Number;
							   if( Frequency > Freq1_Max ) Frequency = Freq1_Max;
								else if( Frequency < Freq1_Min ) Frequency = Freq1_Min;
							    Recalculate_Pwm_Frequency( Frequency );   					// ������������� ������� ���� ����� ������� ������					            
								 Set_Cursor(4,1);
			 					  Send_Lcd_String(Clear_Str);	
								   Send_Lcd_String(Clear_Str);	
				              }	

	 if( Pressed_Key == '*' ){
							   Set_Cursor(4,1);
			 					Send_Lcd_String(Clear_Str);	
								 Send_Lcd_String(Clear_Str);							
							 }	
		Pressed_Key = ' ';				 		   						
																	
}

// ----------------------------------------------------------------------------------------------------------------------------------------------

int Input_Number_From_Keyboard ( int digits, int row, int position){

			  int Number, Total_Digits;

			  Set_Cursor( row, position );
			   Total_Digits = 0; Number = 0;
                Block_Key_Scan_Timer = 0; Flags.Block_Key_Scan_Flag = 1; Pressed_Key = 0;

  while( (Total_Digits < digits) &&  ( Pressed_Key != '*' ) && ( Pressed_Key != '#' )  ) {

		if( ((Scan_Code = Key_Scan()) != 7) && (Flags.Block_Key_Scan_Flag != 1) ){

											Block_Key_Scan_Timer = 0; Flags.Block_Key_Scan_Flag = 1;  // ������ � ���� ��� ���������� ��������
										    Pressed_Key = Key_Decode( Scan_Code );
										     if( (Pressed_Key != '*' ) && ( Pressed_Key != '#' ) ){
																								   Number = Number * 10 + (Pressed_Key - 48 );
																								   Total_Digits++;	
																								   Send_Lcd_Data( Pressed_Key );
																									}																	 									  													

																					}   // end of if( ((Scan_Code = Key_Scan()) .....									
												 	
																					  }	// end of while( (Total_Digits < 5) .........				

	 if( (Pressed_Key == '#' ) || (Total_Digits == digits) ) return( Number);
	  else return(0); 		 		   				

}

// ----------------------------------------------------------------------------------------------------------------------------------------------

void Duty_Cycle_Input(void){

				int Number;

				Set_Cursor(4,1);
			 	 Send_Lcd_String(Duty_Cycle_Str);					// ������� � 4-�� ������ "D=     mks" � ������ � ���������� �������      
			   	   
				Number = Input_Number_From_Keyboard( 5,4,3 );

				if( Number !=0 ){ 
								 Duty_Cycle = Number;   		
								 if( Duty_Cycle > PWM_Duty_Cycle_Max ) Duty_Cycle = (double)PWM_Duty_Cycle_Max;
								
								 if( Frequency_Range == 1) PWM_Duty_Cycle = (int)(Duty_Cycle * 5);
								 if( Frequency_Range == 2) PWM_Duty_Cycle = (int)(Duty_Cycle * 10);
								 if( Frequency_Range == 3) PWM_Duty_Cycle = (int)(Duty_Cycle * 80);
								  
									OC2RS =  PWM_Duty_Cycle;			   // OC2RS - ������� ��� PWM_Duty_Cycle																	
									  Set_Cursor(1,13);
									   sprintf( Converted_String_From_Float, "%7.1f", Duty_Cycle );
										Send_Lcd_String( Clear_Str8 );
										 Set_Cursor(1,13);
										  Send_Lcd_String( Converted_String_From_Float );
									}	
				Set_Cursor(4,1);
			  	 Send_Lcd_String(Clear_Str);	
				  Send_Lcd_String(Clear_Str);	

					Pressed_Key = ' ';			
				
			
}

// -----------------------------------------------------------------------------------------------------------------------------------------------

void Input_Voltage( void ){							// ������ � ���������� �������� ���������� �� 1 �� 1000 ����� 

			int Number;

				Set_Cursor(4,1);
				 Send_Lcd_String( V_Str );
				  Set_Cursor(4,3);

			Number = Input_Number_From_Keyboard( 4,4,3 );

			if( Number != 0 ){
							  Voltage = Number;
							  if( Voltage > Voltage_Max ) Voltage = Voltage_Max;
							  else if( Voltage < Voltage_Min )Voltage = Voltage_Min;
							  Set_Cursor(3,3);
							   sprintf( Converted_String_From_Int, "%5d", Voltage );
								Send_Lcd_String( Clear_Str );
								 Set_Cursor(3,3);
								  Send_Lcd_String( Converted_String_From_Int );					
							    
							  }
	

				Set_Cursor(4,1);
			  	 Send_Lcd_String(Clear_Str);	
				  Send_Lcd_String(Clear_Str);	

					Pressed_Key = ' ';



}


// -------------------------------------------------

BOOL USER_USB_CALLBACK_EVENT_HANDLER(int event, void *pdata, WORD size)
{

	
	
    switch( event )
    {
        case EVENT_TRANSFER:
            //Add application specific callback task or callback function here if desired.
            break;
        case EVENT_SOF:
            //USBCB_SOF_Handler();
            break;
        case EVENT_SUSPEND:
           // USBCBSuspend();
            break;
        case EVENT_RESUME:
           // USBCBWakeFromSuspend();
            break;
        case EVENT_CONFIGURED: 
            USBCBInitEP();
            break;
        case EVENT_SET_DESCRIPTOR:
           // USBCBStdSetDscHandler();
            break;
        case EVENT_EP0_REQUEST:
           // USBCBCheckOtherReq();
            break;
        case EVENT_BUS_ERROR:
            //USBCBErrorHandler();
            break;
        case EVENT_TRANSFER_TERMINATED:
            //Add application specific callback task or callback function here if desired.
            //The EVENT_TRANSFER_TERMINATED event occurs when the host performs a CLEAR
            //FEATURE (endpoint halt) request on an application endpoint which was 
            //previously armed (UOWN was = 1).  Here would be a good place to:
            //1.  Determine which endpoint the transaction that just got terminated was 
            //      on, by checking the handle value in the *pdata.
            //2.  Re-arm the endpoint if desired (typically would be the case for OUT 
            //      endpoints).
            break;            
        default:
            break;
    }      
    return TRUE; 
}

// ---------------------------------------------------------------------------------------------

void USBCBInitEP(void)
{
//	PORTGbits.RG6 = 1;

CDCInitEP();
    //Enable the appplication data endpoint(s)
   // USBEnableEndpoint(USBGEN_EP_NUM,USB_OUT_ENABLED|USB_IN_ENABLED|USB_HANDSHAKE_ENABLED|USB_DISALLOW_SETUP);
    //Prepare the OUT endpoint for the next packet that the host might try to send
   // USBGenericOutHandle = USBGenRead(USBGEN_EP_NUM,(BYTE*)&OUTPacket,USBGEN_EP_SIZE);
}

// ----------------------------------------------------------------------------------------------

void send_rs(void){

if((USBDeviceState < CONFIGURED_STATE)||(USBSuspendControl==1)) return;
																 

   Received_Bytes_Number = getsUSBUSART( (char*)&Rs_Buffer, sizeof(Rs_Buffer) ); 

    		   if( Received_Bytes_Number > 0){
											   Command = Rs_Buffer.Command;
											   Flags.Rs_Data_Ready = 1;									
           									  }

 if( USBUSARTIsTxTrfReady() ){								// �������� ������ �� USB � ��������

						if( (Command > 0) && (Command < 5 )){
								       					     strcpy( Usb_Send_String,"Data Receive OK"); 		 						 
        								    				 putsUSBUSART(Usb_Send_String);
															 }

						if( Command == 5 ) {

							Rs_Buffer.Frequency = Frequency;
							Rs_Buffer.Duty_Cycle = Duty_Cycle;
							Rs_Buffer.Freq1_Max = Freq1_Max;
							Rs_Buffer.Freq1_Min = Freq1_Min;
						    Rs_Buffer.PWM_Duty_Cycle_Max = PWM_Duty_Cycle_Max;
							Rs_Buffer.PWM_Duty_Cycle_Min = PWM_Duty_Cycle_Min;
							Rs_Buffer.PWM_On = PWM_On;

							putUSBUSART( (char*)&Rs_Buffer, sizeof(Rs_Buffer) );	 																				   																			
																			
									 	  }	  // end of   if( Command == 5 )

						  if( Command == 7 ){
											  /* strcpy( Usb_Send_String,"Uart Data received"); 		 						 
        								      putsUSBUSART(Usb_Send_String);

											  Uart_Send_Buffer[0] = Rs_Buffer.HV_Source_Command_Length;												
											  Uart_Send_Buffer[1] = Rs_Buffer.HV_Source_Command;
											  Uart_Send_Buffer[2] = Rs_Buffer.HV_Source_In_Data1;
											  Uart_Send_Buffer[3] = Rs_Buffer.HV_Source_In_Data2;
											  Uart_Send_Buffer[4] = Rs_Buffer.HV_Source_In_Data3;
												*/
											 }					

									}  // if( USBUSARTIsTxTrfReady() 														
												
                								  
							CDCTxService();

}

// ---------------------------------------------------------------------------------------------------


void Command_Processing( void ){  				// ��������� ������� ���������� �� USB �� �����   

								int i;

	if( (Flags.Rs_Data_Ready == 1)&&(Command == 1 ) ){
													Flags.Rs_Data_Ready = 0;
													Frequency = Rs_Buffer.Frequency;
													if( Frequency > Freq1_Max ) Frequency = Freq1_Max;
													if( Frequency < Freq1_Min ) Frequency = Freq1_Min;
													Recalculate_Pwm_Frequency( Frequency );   // ������������� ������� ���� ����� ������� �������
													}

	if( (Flags.Rs_Data_Ready == 1)&&(Command == 2 ) ){
													Flags.Rs_Data_Ready = 0;
													Duty_Cycle = Rs_Buffer.Duty_Cycle;											   		
								 					if( Duty_Cycle > PWM_Duty_Cycle_Max ) Duty_Cycle = PWM_Duty_Cycle_Max;
													else if ( Duty_Cycle < PWM_Duty_Cycle_Min ) Duty_Cycle = PWM_Duty_Cycle_Min;

								 					if( Frequency_Range == 1) PWM_Duty_Cycle = (int)(Duty_Cycle * 5);
								 					if( Frequency_Range == 2) PWM_Duty_Cycle = (int)(Duty_Cycle * 10);
												    if( Frequency_Range == 3) PWM_Duty_Cycle = (int)(Duty_Cycle * 80);														
	  
													  OC2RS =  PWM_Duty_Cycle;			   // OC2RS - ������� ��� PWM_Duty_Cycle																	
									 				   Set_Cursor(1,13);
									   					sprintf( Converted_String_From_Float, "%7.1f", Duty_Cycle );
														 Send_Lcd_String( Clear_Str8 );
													 	  Set_Cursor(1,13);
										 				   Send_Lcd_String( Converted_String_From_Float );
													 }

    if( (Flags.Rs_Data_Ready == 1)&&(Command == 4 ) ){
													  PWM_On = PWM_On ^ 1;	
													   T2CONbits.ON = 0; 
														a++; 
														 TMR2 = 0;																
														  OC2CON = OC2CON ^ 0x00008000;

															if( (OC2CON & 0x00008000)>>15 ) T2CONbits.ON = 1;
	
															  Set_Cursor(4,13);
															   Send_Lcd_String( Clear_Str8 );
																Set_Cursor(4,13);

																  if( (OC2CON & 0x00008000)>>15 ){ Send_Lcd_String( PWM_On_Str);  }
																	 else Send_Lcd_String( PWM_Off_Str);
															}

	  if( (Flags.Rs_Data_Ready == 1)&&(Command == 7 ) ){

											// if( U2STAbits.TRMT == 1) U2TXREG = 0;

				for( i = 0; i <= Rs_Buffer.HV_Source_Command_Length; i++ ){	
		
														 while( U2STAbits.TRMT != 1){}
											 			  U2TXREG = Uart_Send_Buffer[i];							

																		}	

														}																					

									

	Flags.Rs_Data_Ready = 0;
    Command = 0;

}	

// ---------------------------------------------------------------------------------------------------
	
void First_Screen( void ){

if( (First_Screen_Timer == 3)&&(Flags.First_Screen_Flag  == 0) ) {
				
											Flags.First_Screen_Flag = 1;
											 Send_Lcd_Command(0x01);
												Send_Lcd_String(s1);
												 Set_Cursor(1,11);
												  Send_Lcd_String(s2);	
												   Set_Cursor(2,1);
													Send_Lcd_String(Dmin_Str);
													 Set_Cursor(2,11);
													   Send_Lcd_String(Dmax_Str);	

									//if( Frequency_Range == 1 ) Frequency = Freq1_Min;
									//else if( Frequency_Range == 2) Frequency = Freq2_Min;
									//else if( Frequency_Range == 3) Frequency = Freq3_Min;
									
									  Set_Cursor(1,3);
									   sprintf( Converted_String_From_Int, "%5d", Frequency );	
									   	Send_Lcd_String( Converted_String_From_Int );
									     Set_Cursor(1,13);
										  sprintf( Converted_String_From_Float, "%7.1f", Duty_Cycle );	
								           Send_Lcd_String( Converted_String_From_Float );
											Set_Cursor(2,6);
											 sprintf( Converted_String_From_Int, "%d", PWM_Duty_Cycle_Min );	
								              Send_Lcd_String( Converted_String_From_Int );
											   Set_Cursor(2,16);
											  	sprintf( Converted_String_From_Int, "%5d", PWM_Duty_Cycle_Max );	
								             	 Send_Lcd_String( Converted_String_From_Int );
												  Set_Cursor(3,1);
												   Send_Lcd_String( V_Str );
													Set_Cursor(3,3);
													 sprintf( Converted_String_From_Int, "%5d", Voltage );
													   Send_Lcd_String( Converted_String_From_Int );
														 Set_Cursor(3,11);
														   Send_Lcd_String( Ur_Str );

															Set_Cursor(4,13);
														    Send_Lcd_String( Clear_Str8 );
															Set_Cursor(4,13);
	
															if( (OC2CON & 0x00008000)>>15 ){ Send_Lcd_String( PWM_On_Str);  }
															  else Send_Lcd_String( PWM_Off_Str);		
												

															}   // end of if( (First_Screen_Timer == 3 ....	

	}

// ---------------------------------------------------------------------------------------------------

void Keys_Processing(void){

if (Flags.First_Screen_Flag )
 if( (Flags.Key_Scan_Flag == 1 ) && ( Flags.Block_Key_Scan_Flag != 1) ){

							 Pressed_Key == ' ';

							 Scan_Code = Key_Scan();

								if( Scan_Code != 7 ){ 
													  Display_Light_Timer = 0;
													   Display_Light_On = 1;     // ����� ������� ������� �������� ��������� ������� �� 30 ������


									Pressed_Key = Key_Decode( Scan_Code );


													//Set_Cursor(4,14);
								   					// Send_Lcd_String("     ");
								   					//  Set_Cursor(4,14);
								   					//   sprintf( Converted_String_From_Int, "%d", Pressed_Key );
								   					//	Send_Lcd_String( Converted_String_From_Int );

								
									   if( Pressed_Key == '3' ){
																 Frequency = Frequency + 1;
																  if( Frequency > Freq1_Max ) Frequency = Freq1_Max;
															 	   Recalculate_Pwm_Frequency( Frequency );   // ������������� ������� ���� ����� ������� �������
																    
																}

										if( Pressed_Key == '1' ){
																 Frequency = Frequency - 1;
																  if( Frequency < Freq1_Min ) Frequency = Freq1_Min;
															 	   Recalculate_Pwm_Frequency( Frequency );   		// ������������� ������� ���� ����� ������� �������

											/*	if(	Wait_Until_Bytes_Not_Read == 0 ) {

																Uart_Send_Byte = 0x03;
																while( U2STAbits.TRMT == 0){}
											 			  		U2TXREG = Uart_Send_Byte;

																Uart_Send_Byte = 0x03;
																while( U2STAbits.TRMT ==0){}
											 			  		U2TXREG = Uart_Send_Byte;

																Uart_Send_Byte = 0x00;
																while( U2STAbits.TRMT ==0){}
											 			  		U2TXREG = Uart_Send_Byte;

																Uart_Send_Byte = 0x60;
																while( U2STAbits.TRMT ==0){}
											 			  		U2TXREG = Uart_Send_Byte;

																Uart_Command_Was_Send_Flag = 1;

																Wait_Until_Bytes_Not_Read = 1;

																Timeout_Timer = 0;
     															Timeout_Flag = 1;
	
																					}	*/									

																    
																}

										 if( Pressed_Key == '6' ){ 					// ����������� PWM_Duty_Cycle �� ���� ������������

																if( Frequency_Range == 1 ){
																  						   Duty_Cycle = Duty_Cycle + 0.2;   			// ��� �� ��� ������������ �� ������ � �������������
																  						   PWM_Duty_Cycle = PWM_Duty_Cycle + 1;
																						  }	

																else if( Frequency_Range == 2 ){
																  						        Duty_Cycle = Duty_Cycle + 0.1;   			// ��� �� ��� ������������ �� ������ � �������������
																  						        PWM_Duty_Cycle = PWM_Duty_Cycle + 1;
																						       }	

																else if( Frequency_Range == 3 ){
																  						        Duty_Cycle = Duty_Cycle + 0.1;   			// ��� �� ��� ������������ �� ������ � �������������
																  						        PWM_Duty_Cycle = PWM_Duty_Cycle + 8;
																						       }						

															    if( Duty_Cycle > PWM_Duty_Cycle_Max ){
																									  Duty_Cycle = (double)PWM_Duty_Cycle_Max;
																									  if( Frequency_Range == 1 ) PWM_Duty_Cycle = (int)(Duty_Cycle * 5);
																									  if( Frequency_Range == 2 ) PWM_Duty_Cycle = (int)(Duty_Cycle * 10);
																									  if( Frequency_Range == 3 ) PWM_Duty_Cycle = (int)(Duty_Cycle * 80);
																									 }						
																       
																	 OC2RS =  PWM_Duty_Cycle;			   // OC2RS - ������� ��� PWM_Duty_Cycle																	
																      Set_Cursor(1,13);
													  				   sprintf( Converted_String_From_Float, "%7.1f", Duty_Cycle );
																	    Send_Lcd_String( Clear_Str8 );
																	     Set_Cursor(1,13);
																		  Send_Lcd_String( Converted_String_From_Float );																		 
																}	

										  if( Pressed_Key == '4' ){ 				// ��������� PWM_Duty_Cycle �� ���� ������������

																  if( Frequency_Range == 1 ){
																  						     Duty_Cycle = Duty_Cycle - 0.2;   			// ��� �� ��� ������������ �� ������ � �������������
																  						     PWM_Duty_Cycle = PWM_Duty_Cycle - 1;
																						    }	

																  else if( Frequency_Range == 2 ){
																  						          Duty_Cycle = Duty_Cycle - 0.1;   			// ��� �� ��� ������������ �� ������ � �������������
																  						          PWM_Duty_Cycle = PWM_Duty_Cycle - 1;
																						         }	

																  else if( Frequency_Range == 3 ){
																  						          Duty_Cycle = Duty_Cycle - 0.1;   			// ��� �� ��� ������������ �� ������ � �������������
																  						          PWM_Duty_Cycle = PWM_Duty_Cycle - 8;
																						         }						

															      if( Duty_Cycle < PWM_Duty_Cycle_Min ){
																									  Duty_Cycle = (double)PWM_Duty_Cycle_Min;
																									  if( Frequency_Range == 1 ) PWM_Duty_Cycle = (int)(Duty_Cycle * 5);
																									  if( Frequency_Range == 2 ) PWM_Duty_Cycle = (int)(Duty_Cycle * 10);
																									  if( Frequency_Range == 3 ) PWM_Duty_Cycle = (int)(Duty_Cycle * 80);
																									 }

																	OC2RS =  PWM_Duty_Cycle;			   // OC2RS - ������� ��� PWM_Duty_Cycle																	
																     Set_Cursor(1,13);
													  				  sprintf( Converted_String_From_Float, "%7.1f", Duty_Cycle );
																	   Send_Lcd_String( Clear_Str8 );
																	    Set_Cursor(1,13);
																		 Send_Lcd_String( Converted_String_From_Float );																		 
																  }			
	  			
	
										  if( Pressed_Key == '2' ){
 															 		 Frequency_Input();
																	}				

										  if( Pressed_Key == '5' ) Duty_Cycle_Input();


										  if( Pressed_Key == '#' ){

														if( PWM_Was_On == 0 ){
																			  TMR2 = 0; TMR3 = 0; 
																              PWM_On = 1;	
																   			  T2CONbits.ON = 1; 
																			  T3CONbits.ON = 1;
																			  a++;  		  															
																	  		  OC2CON = OC2CON | 0x00008000;
																			  OC1CON = OC1CON | 0x00008000;

																			  Set_Cursor(4,13);
																              Send_Lcd_String( Clear_Str8 );
																              Set_Cursor(4,13);
																			  Send_Lcd_String( PWM_On_Str);
																			  PWM_Was_On = 1; 
																			 }
	
														 else if( PWM_Was_On == 1 ){ 
																              PWM_On = 0;	
																   			  T2CONbits.ON = 0; 
																			  T3CONbits.ON = 0;
																			  a++;  		  															
																	  		  OC2CON = OC2CON &~ 0x00008000;
																			  OC1CON = OC1CON &~ 0x00008000;

																			  Set_Cursor(4,13);
																              Send_Lcd_String( Clear_Str8 );
																              Set_Cursor(4,13);
																			  Send_Lcd_String( PWM_Off_Str);
																			  PWM_Was_On = 0; 
																			 }					
													
																	}
	

									    	if( Pressed_Key == '9' ){
																	 Voltage = Voltage + 1;
																	  if( Voltage > Voltage_Max ) Voltage = Voltage_Max;																			
													  				      Set_Cursor(3,3);
							                                               sprintf( Converted_String_From_Int, "%5d", Voltage );
								                                            Send_Lcd_String( Clear_Str );
								                                             Set_Cursor(3,3);
								                                              Send_Lcd_String( Converted_String_From_Int );																	
																	}


											if( Pressed_Key == '7' ){
																	 Voltage = Voltage - 1;
																	  if( Voltage < Voltage_Min ) Voltage = Voltage_Min;
																	   Set_Cursor(3,3);
							                                            sprintf( Converted_String_From_Int, "%5d", Voltage );
								                                         Send_Lcd_String( Clear_Str );
								                                             Set_Cursor(3,3);
								                                              Send_Lcd_String( Converted_String_From_Int );			
																	}

											if( Pressed_Key == '8' ){
																	 Input_Voltage();		
																	}


											if( Pressed_Key == '*' ){
																	 /*float x_float;
																	  x_float = 50067.12872;
																	   sprintf( Converted_String_From_Float , "%7.1f", Duty_Cycle );	
																		Set_Cursor(3,1);
																		 Send_Lcd_String( Clear_Str );	
																		  Send_Lcd_String( Clear_Str );
																		   Set_Cursor(3,1); 	
																			Send_Lcd_String( Converted_String_From_Float );*/
																				//	Send_Lcd_String( Converted_String_From_Int );
																	}

											if( Pressed_Key == 11 ){
								 									 Window2();
																	}

											if( Pressed_Key == 12 ){
								 									Time_Date_Setup_Window();
																	}

											if( Pressed_Key == 13 ){
								 									Window3();
																	}
																		
			
													 
								         	Flags.Block_Key_Scan_Flag = 1; Block_Key_Scan_Timer = 0;
						
													}	// end of 	if( Scan_Code != 7 )																						
							
											Flags.Key_Scan_Flag = 0;

								}				// end of  if( Flags.Key_Scan_Flag == 1 )						

}

// ---------------------------------------------------------------------------------------------------

void Window2(){
					
			  Send_Lcd_Command(0x01);
			  Send_Lcd_String( "    Time and ADC" );

			  Milisek_Delay(500);

			  Set_Cursor(3,1);
			  Send_Lcd_String("AN1=");

			  Set_Cursor(3,12);
			  Send_Lcd_String("AN2=");

			  Set_Cursor(4,1);
			  Send_Lcd_String("AN3=");

			  Set_Cursor(4,12);
			  Send_Lcd_String("AN4=");

									Set_Cursor(2,1);

									sprintf( Converted_String_From_Int, "%2d", I2C_Data_Hours );
									Send_Lcd_String( Converted_String_From_Int );
									Send_Lcd_String( ":" );

									sprintf( Converted_String_From_Int, "%2d", I2C_Data_Minutes);
									Send_Lcd_String( Converted_String_From_Int );
									Send_Lcd_String( ":" );
									if( I2C_Data_Minutes < 10 ){ Set_Cursor(2,4); Send_Lcd_String("0"); }

									sprintf( Converted_String_From_Int, "%2d", I2C_Data_Sekonds );
									Send_Lcd_String( Converted_String_From_Int );

									if( I2C_Data_Hours < 10 ){ Set_Cursor(2,1); Send_Lcd_String("0"); }
								    if( I2C_Data_Minutes < 10 ){ Set_Cursor(2,4); Send_Lcd_String("0"); }
									if( I2C_Data_Sekonds < 10 ){ Set_Cursor(2,7); Send_Lcd_String("0"); }

									Set_Cursor(2,9);
			
								
			Flags.Key_Scan_Flag = 0; Key_Scan_Timer = 0;
			 Flags.Block_Key_Scan_Flag = 1; Block_Key_Scan_Timer = 0;
		
			   Pressed_Key == ' ';

 while(1){

	if( Time_Date_Ready_Flag == 1 ){
									Set_Cursor(2,1);

									sprintf( Converted_String_From_Int, "%2d", I2C_Data_Hours );
									Send_Lcd_String( Converted_String_From_Int );
									Send_Lcd_String( ":" );
								
									sprintf( Converted_String_From_Int, "%2d", I2C_Data_Minutes);
									Send_Lcd_String( Converted_String_From_Int );
									Send_Lcd_String( ":" );

									sprintf( Converted_String_From_Int, "%2d", I2C_Data_Sekonds );
									Send_Lcd_String( Converted_String_From_Int );

									if( I2C_Data_Hours < 10 ){ Set_Cursor(2,1); Send_Lcd_String("0"); }
								    if( I2C_Data_Minutes < 10 ){ Set_Cursor(2,4); Send_Lcd_String("0"); }
									if( I2C_Data_Sekonds < 10 ){ Set_Cursor(2,7); Send_Lcd_String("0"); }

									Set_Cursor(2,9);

									Set_Cursor(2,12);
	
									sprintf( Converted_String_From_Int, "%2d", Day );
									Send_Lcd_String( Converted_String_From_Int );
									Send_Lcd_String( "-" );									

									sprintf( Converted_String_From_Int, "%2d", Month);
									Send_Lcd_String( Converted_String_From_Int );
									Send_Lcd_String( "-" );
									
									sprintf( Converted_String_From_Int, "%2d", Year );
									Send_Lcd_String( Converted_String_From_Int );

									if( Day < 10 ){ Set_Cursor(2,12); Send_Lcd_String("0"); }
									if( Month < 10 ){ Set_Cursor(2,15); Send_Lcd_String("0"); }
									if( Year < 10 ){ Set_Cursor(2,18); Send_Lcd_String("0"); }
									
									Set_Cursor(2,20);		

									Time_Date_Ready_Flag = 0; 
									}

	if( (Flags.Key_Scan_Flag == 1 ) && ( Flags.Block_Key_Scan_Flag != 1) ){         // ������������ ����������

							 Pressed_Key == ' ';

							 Scan_Code = Key_Scan();

								if( Scan_Code != 7 ){ 
													  Display_Light_Timer = 0;
													   Display_Light_On = 1;     // ����� ������� ������� �������� ��������� ������� �� 30 ������	

									        Pressed_Key = Key_Decode( Scan_Code );

										    Flags.Block_Key_Scan_Flag = 1; Block_Key_Scan_Timer = 0;
						
													 }	// end of 	if( Scan_Code != 7 )

											Flags.Key_Scan_Flag = 0; Key_Scan_Timer = 0;

																			}				// end of  if( Flags.Key_Scan_Flag == 1 )

	if( Pressed_Key == '*' ) break;

	/*
	if(	Flags.Adc_Data_Ready == 1 ){
								    Set_Cursor(2,5);

									sprintf( Converted_String_From_Int, "%4d", 	Adc_Channel1);
									Send_Lcd_String( Converted_String_From_Int );
								
									Flags.Adc_Data_Ready = 0;
								    }
								*/

	if( Flags.Adc_Convert_Flag == 1 ){

									  Scan_Adc_Channels();

 									  Set_Cursor(3,5);
									  sprintf( Converted_String_From_Int, "%4d", 	Adc_Channel1);
									  Send_Lcd_String( Converted_String_From_Int );

									  Set_Cursor(3,16);
									  sprintf( Converted_String_From_Int, "%4d", 	Adc_Channel2);
									  Send_Lcd_String( Converted_String_From_Int );
	
									  Set_Cursor(4,5);
									  sprintf( Converted_String_From_Int, "%4d", 	Adc_Channel3);
									  Send_Lcd_String( Converted_String_From_Int );

									  Set_Cursor(4,16);
									  sprintf( Converted_String_From_Int, "%4d", 	Adc_Channel4);
									  Send_Lcd_String( Converted_String_From_Int );

									  Flags.Adc_Convert_Flag = 0;
									  Adc_Timer = 0; 
									  Flags.Adc_Data_Ready = 1;
									
									 }								

			}	//  end of while								


	    First_Screen_Timer = 3;
		 Flags.First_Screen_Flag  = 0; 

}

	  
// ----------------------------------------------------------------------------------------------------------------------------------------------

void Get_Timer(){


													Ack_Flag = 0;
								
													I2C_Flag = 9;										

													I2C1CONbits.SEN = 1;            		// Start bit													
													while( I2C1CONbits.SEN == 1 ){}
											
													I2C_Delay(5);
												
													I2C1TRN = 208;     						// �������� ����� ����� � ��� ������ � �����
												    while( I2C1STATbits.TRSTAT == 1 ){}
													if ( I2C1STATbits.ACKSTAT == 0) Ack_Flag++;
			
													I2C_Delay(10);

													I2C1TRN = 0;   							// ����� �������� ������ �����
													while( I2C1STATbits.TRSTAT == 1 ){}
													if ( I2C1STATbits.ACKSTAT == 0) Ack_Flag++; 
												
													I2C_Delay(10);
																							
													I2C1CONbits.RSEN = 1;    				// Repeated Start Transfer from Master to slave																				   																								
													while( I2C1CONbits.RSEN == 1 ){}

													//I2C_Delay(25);													
													
													I2C1TRN = 209;     						// �������� ����� ����� � ��� ������ �� �����
												    while( I2C1STATbits.TRSTAT == 1 ){}
													if ( I2C1STATbits.ACKSTAT == 0) Ack_Flag++;
	
													I2C_Delay(10);

													// -- ������ ������� ---

													I2C1CONbits.RCEN = 1;
													a = 0; 
													I2C_Delay(25);
													while( I2C1STATbits.RBF != 1 ){
																					a++;
																					if(a >=500)break;
																				}
													I2C1CONbits.ACKDT = 0;
													I2C1CONbits.ACKEN = 1;						
													I2C_Data_Sekonds = I2C1RCV;
													I2C_Delay(25);	
											
													// -- ������ ������ ---
										
													I2C1CONbits.RCEN = 1;
													a = 0; 
													I2C_Delay(25);
													while( I2C1STATbits.RBF != 1 ){
																					a++;
																					if(a >=500)break;
																					}
													I2C1CONbits.ACKDT = 0;
													I2C1CONbits.ACKEN = 1;					
													I2C_Data_Minutes = I2C1RCV;
													I2C_Delay(25);	
												
																						 
													// -- ������ ���� ---

													I2C1CONbits.RCEN = 1; 
													a = 0;
													I2C_Delay(25);
													while( I2C1STATbits.RBF != 1 ){
																					a++;
																					if(a >=500)break;
																					}
													I2C1CONbits.ACKDT = 0;
													I2C1CONbits.ACKEN = 1;
													I2C_Data_Hours = I2C1RCV;
													I2C_Delay(25);

													///////// ���  ������  ////////////

													I2C1CONbits.RCEN = 1; 
													a = 0;
													I2C_Delay(25);
													while( I2C1STATbits.RBF != 1 ){
																					a++;
																					if(a >=500)break;
																					}
													I2C1CONbits.ACKDT = 0;
													I2C1CONbits.ACKEN = 1;
													Day_Number = I2C1RCV;
													I2C_Delay(25);

													////// ����� //////////////////////													

													I2C1CONbits.RCEN = 1; 
													a = 0;
													I2C_Delay(25);
													while( I2C1STATbits.RBF != 1 ){
																					a++;
																					if(a >=500)break;
																					}
													I2C1CONbits.ACKDT = 0;
													I2C1CONbits.ACKEN = 1;
													Day = I2C1RCV;
													I2C_Delay(25);
														
													////// ����� //////////////////////
						
													I2C1CONbits.RCEN = 1; 
													a = 0;
													I2C_Delay(25);
													while( I2C1STATbits.RBF != 1 ){
																					a++;
																					if(a >=500)break;
																					}
													I2C1CONbits.ACKDT = 0;
													I2C1CONbits.ACKEN = 1;
													Month = I2C1RCV;
													I2C_Delay(25);

													////// ��� //////////////////////

													I2C1CONbits.RCEN = 1; 
													a = 0;
													I2C_Delay(25);
													while( I2C1STATbits.RBF != 1 ){
																					a++;
																					if(a >=500)break;
																					}
													I2C1CONbits.ACKDT = 1;
													I2C1CONbits.ACKEN = 1;
													Year = I2C1RCV;
													I2C_Delay(25);	
																		 
																			

													I2C1CONbits.PEN = 1; 				// Stop Receive
													//	I2C1CONbits.PEN = 1; 			// Stop Receive

										I2C_Data_Sekonds = ( (I2C_Data_Sekonds & 0xF0 ) >> 4 )*10 + ( I2C_Data_Sekonds & 0x0F );
										I2C_Data_Minutes = ( (I2C_Data_Minutes & 0xF0 ) >> 4 )*10 + ( I2C_Data_Minutes & 0x0F );
										I2C_Data_Hours = ( (I2C_Data_Hours & 0xF0 ) >> 4 )*10 + ( I2C_Data_Hours & 0x0F );

										Day = ( (Day & 0xF0 ) >> 4 )*10 + ( Day & 0x0F );
										Month = ( (Month & 0xF0 ) >> 4 )*10 + ( Month & 0x0F );
										Year = ( (Year & 0xF0 ) >> 4 )*10 + ( Year & 0x0F );
 
										Current_Hour = I2C_Data_Hours;
										Current_Minutes = I2C_Data_Minutes;  

												//	Get_Adc_Data_Flag = 0;
										  		//	Adc_Timer = 0;

}

// ------------------------------------------------------------------------------------------------------------------------------------

void I2C_Delay( int Delay ){
							int k;
							 for( k = 0; k < Delay ; k++ ); 
}

// ------------------------------------------------------------------------------------------------------------------------------------

void I2C2_Delay( int Delay ){
							int k;
							 for( k = 0; k < Delay ; k++ ); 
}


// ------------------------------------------------------------------------------------------------------------------------------------

void Save_Time(unsigned char hours, unsigned char mins){

													unsigned char Hours_Bcd, Mins_Bcd;

													//Hours_H = hours / 10; Hours_L = hours % 10; Hours_Bcd = ( Hours_H << 4 ) | Hours_L;

													Hours_Bcd = ( ( hours / 10 ) << 4 ) | ( hours % 10 );
													Mins_Bcd = ( ( mins / 10 ) << 4 ) | ( mins % 10 );


													///////////////////////////////

													I2C_Flag = 9;										

													I2C1CONbits.SEN = 1;            		// Start bit													
													while( I2C1CONbits.SEN == 1 ){}
													
													I2C_Flag = 10;				

													I2C1TRN = 208;     						// �������� ����� �����
												    while( I2C1STATbits.TRSTAT == 1 ){}

													I2C_Flag = 11; 	a++; a++; a++;

													if ( I2C1STATbits.ACKSTAT == 0){ I2C_Flag = 17; Ack_Flag = 1; }
															else I2C_Flag = 16;; 

													I2C1TRN = 0;   				    // ����� �������� ������ �����
													while( I2C1STATbits.TRSTAT == 1 ){}
													if ( I2C1STATbits.ACKSTAT == 0){ I2C_Flag = 18; Ack_Flag++; }
												
													a++; a++; a++;

													I2C1TRN = 0x00;   				// �������� ������� �����
													while( I2C1STATbits.TRSTAT == 1 ){}
													if ( I2C1STATbits.ACKSTAT == 0){ I2C_Flag = 18; Ack_Flag++; }
												
													a++; a++; a++;

													I2C1TRN = Mins_Bcd;   				// �������� ������ �����
													while( I2C1STATbits.TRSTAT == 1 ){}
													if ( I2C1STATbits.ACKSTAT == 0){ I2C_Flag = 18; Ack_Flag++; }
												
													a++; a++; a++;

													I2C1TRN = Hours_Bcd;   				// �������� ���� �����
													while( I2C1STATbits.TRSTAT == 1 ){}
													if ( I2C1STATbits.ACKSTAT == 0){ I2C_Flag = 18; Ack_Flag++; }
												
													a++; a++; a++;

													I2C1CONbits.PEN = 1; 				// Stop Receive




}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------  

void Time_Date_Setup_Window(){

							unsigned char Result, Position;
						    char Tmp_Hours, Tmp_Minutes, Tmp_Seconds, Old_Tmp_Hours, Old_Tmp_Minutes, Old_Tmp_Sekonds;
							char Tmp_Day, Tmp_Month, Tmp_Year, Old_Tmp_Day, Old_Tmp_Month, Old_Tmp_Year; 

							Position = 1; Result = 0;

							Old_Tmp_Hours = Tmp_Hours = I2C_Data_Hours;
							Old_Tmp_Minutes = Tmp_Minutes = I2C_Data_Minutes;
							Old_Tmp_Sekonds = Tmp_Seconds = I2C_Data_Sekonds;
					
							Old_Tmp_Day = Tmp_Day = Day;
							Old_Tmp_Month = Tmp_Month = Month;
 							Old_Tmp_Year = Tmp_Year = Year;
							

							 Send_Lcd_Command(0x01);
			  				 Send_Lcd_String( "Time and Date Setup" );

							        Set_Cursor(3,1);

									sprintf( Converted_String_From_Int, "%2d", Tmp_Hours );
									Send_Lcd_String( Converted_String_From_Int );
									Send_Lcd_String( ":" );
								
									sprintf( Converted_String_From_Int, "%2d", Tmp_Minutes);
									Send_Lcd_String( Converted_String_From_Int );
									Send_Lcd_String( ":" );

									sprintf( Converted_String_From_Int, "%2d", Tmp_Seconds );
									Send_Lcd_String( Converted_String_From_Int );

									if( I2C_Data_Hours < 10 ){ Set_Cursor(3,1); Send_Lcd_String("0"); }
								    if( I2C_Data_Minutes < 10 ){ Set_Cursor(3,4); Send_Lcd_String("0"); }
									if( I2C_Data_Sekonds < 10 ){ Set_Cursor(3,7); Send_Lcd_String("0"); }

									Set_Cursor(3,9);

									Set_Cursor(3,12);
	
									sprintf( Converted_String_From_Int, "%2d", Tmp_Day );
									Send_Lcd_String( Converted_String_From_Int );
									Send_Lcd_String( "-" );									

									sprintf( Converted_String_From_Int, "%2d", Tmp_Month);
									Send_Lcd_String( Converted_String_From_Int );
									Send_Lcd_String( "-" );
									
									sprintf( Converted_String_From_Int, "%2d", Tmp_Year );
									Send_Lcd_String( Converted_String_From_Int );

									if( Day < 10 ){ Set_Cursor(3,12); Send_Lcd_String("0"); }
									if( Month < 10 ){ Set_Cursor(3,15); Send_Lcd_String("0"); }
									if( Year < 10 ){ Set_Cursor(3,18); Send_Lcd_String("0"); }
									
									Set_Cursor(3,20);		
										
			Flags.Key_Scan_Flag = 0; Key_Scan_Timer = 0;
			 Flags.Block_Key_Scan_Flag = 1; Block_Key_Scan_Timer = 0;

			  Set_Cursor(3,2);

 while(1){


   if( (Flags.Key_Scan_Flag == 1 ) && ( Flags.Block_Key_Scan_Flag != 1) ){         // ������������ ����������

							    Pressed_Key = ' ';
							    Scan_Code = Key_Scan();

								if( Scan_Code != 7 ){ 
													 Display_Light_Timer = 0;
													 Display_Light_On = 1;        // ����� ������� ������� �������� ��������� ������� �� 30 ������	

									        		 Pressed_Key = Key_Decode( Scan_Code );

										             Flags.Block_Key_Scan_Flag = 1; Block_Key_Scan_Timer = 0;
						
													 }	// end of 	if( Scan_Code != 7 )

														Flags.Key_Scan_Flag = 0; Key_Scan_Timer = 0;

																			}				// end of  if( Flags.Key_Scan_Flag == 1 )

						if( Pressed_Key == '*' ) break;
					
						if( Pressed_Key == '#' ){ 
												 Block_Get_Timer = 1;
												 Save_Time( Tmp_Hours, Tmp_Minutes );
												 Milisek_Delay(200);
												 Save_Date( Tmp_Day, Tmp_Month, Tmp_Year );
												 Milisek_Delay(200);
												 Block_Get_Timer = 0;
												 break;
												}

						if( Pressed_Key == '6' ){       									// ���������� ������� ������� ������
												 Position++;
												 if( Position > 6 ) Position = 1;
												 if( Position == 1 )Set_Cursor( 3, 2 );
												 else if ( Position == 2 )Set_Cursor( 3, 5);
												 else if ( Position == 3 )Set_Cursor( 3, 8 );
												 else if ( Position == 4 )Set_Cursor( 3, 13 );
												 else if ( Position == 5 )Set_Cursor( 3, 16 );
												 else if ( Position == 6 )Set_Cursor( 3, 19 );
	
																	}

						if( Pressed_Key == '4' ){       									// ���������� ������� ������� �����
												 Position--;
												 if( Position < 1 ) Position = 6;
												 if( Position == 1 )Set_Cursor( 3, 2 );
												 else if ( Position == 2 )Set_Cursor( 3, 5);
												 else if ( Position == 3 )Set_Cursor( 3, 8 );
												 else if ( Position == 4 )Set_Cursor( 3, 13 );
												 else if ( Position == 5 )Set_Cursor( 3, 16 );
												 else if ( Position == 6 )Set_Cursor( 3, 19 );
	
												}						
									
							
						if( Pressed_Key == '2' ){
												 if( Position == 1 ){
																 	  Tmp_Hours++;	 
					 									 		 	  if( Tmp_Hours > 23 ) Tmp_Hours = 0;						 									
							  								 	 	  sprintf( Converted_Int, "%2d", Tmp_Hours );
				      		   						 		   		  Set_Cursor(3,1);	
				      										   	 	  Send_Lcd_String( Converted_Int );	
																	  if( Tmp_Hours < 10 ){ Set_Cursor(3,1); Send_Lcd_String("0"); }							 		
										 						 	  Set_Cursor(3,2);
																	 }

												  if( Position == 2 ){
																 	  Tmp_Minutes++;	 
					 									 		 	  if( Tmp_Minutes > 59 ) Tmp_Minutes = 0;						 									
							  								 	 	  sprintf( Converted_Int, "%2d", Tmp_Minutes );
				      		   						 		   		  Set_Cursor(3,4);	
				      										   	 	  Send_Lcd_String( Converted_Int );	
																	  if( Tmp_Minutes < 10 ){ Set_Cursor(3,4); Send_Lcd_String("0"); }							 		
										 						 	  Set_Cursor(3,5);
																	 }

												  if( Position == 3 ){
																 	  Tmp_Seconds++;	 
					 									 		 	  if( Tmp_Seconds > 59 ) Tmp_Seconds = 0;						 									
							  								 	 	  sprintf( Converted_Int, "%2d", Tmp_Seconds );
				      		   						 		   		  Set_Cursor(3,7);	
				      										   	 	  Send_Lcd_String( Converted_Int );	
																	  if( Tmp_Seconds < 10 ){ Set_Cursor(3,7); Send_Lcd_String("0"); }							 		
										 						 	  Set_Cursor(3,8);
																	 }

												  if( Position == 4 ){
																 	  Tmp_Day++;	 
					 									 		 	  if( Tmp_Day > 31 ) Tmp_Day = 1;						 									
							  								 	 	  sprintf( Converted_Int, "%2d", Tmp_Day );
				      		   						 		   		  Set_Cursor(3,12);	
				      										   	 	  Send_Lcd_String( Converted_Int );	
																	  if( Tmp_Day < 10 ){ Set_Cursor(3,12); Send_Lcd_String("0"); }							 		
										 						 	  Set_Cursor(3,13);
																	 }
																		
												  if( Position == 5 ){
																 	  Tmp_Month++;	 
					 									 		 	  if( Tmp_Month > 12 ) Tmp_Month = 1;						 									
							  								 	 	  sprintf( Converted_Int, "%2d", Tmp_Month );
				      		   						 		   		  Set_Cursor(3,15);	
				      										   	 	  Send_Lcd_String( Converted_Int );	
																	  if( Tmp_Month < 10 ){ Set_Cursor(3,15); Send_Lcd_String("0"); }							 		
										 						 	  Set_Cursor(3,16);
																	 }

												  if( Position == 6 ){
																 	  Tmp_Year++;	 
					 									 		 	  if( Tmp_Year > 99 ) Tmp_Year = 0;						 									
							  								 	 	  sprintf( Converted_Int, "%2d", Tmp_Year );
				      		   						 		   		  Set_Cursor(3,18);	
				      										   	 	  Send_Lcd_String( Converted_Int );	
																	  if( Tmp_Year < 10 ){ Set_Cursor(3,18); Send_Lcd_String("0"); }							 		
										 						 	  Set_Cursor(3,19);
																	 }
																    
																}

										
					
																   
										Pressed_Key = ' ';
																}  

		       First_Screen_Timer = 3;
		       Flags.First_Screen_Flag  = 0; 
											
  }


// -------------------------------------------------------------------------------------------------------------------------------

void Save_Date(unsigned char day, unsigned char month, unsigned char year){

													unsigned char Day_Bcd, Month_Bcd, Year_Bcd;

													Day_Bcd = ( ( day / 10 ) << 4 ) | ( day % 10 );
													Month_Bcd = ( ( month / 10 ) << 4 ) | ( month % 10 );
													Year_Bcd = ( ( year / 10 ) << 4 ) | ( year % 10 );
			
													///////////////////////////////

													I2C_Flag = 9;										

													I2C1CONbits.SEN = 1;            		// Start bit													
													while( I2C1CONbits.SEN == 1 ){}
													
													I2C_Flag = 10;				

													I2C1TRN = 208;     						// �������� ����� �����
												    while( I2C1STATbits.TRSTAT == 1 ){}
													I2C_Flag = 11; 	a++; a++; a++;

													if ( I2C1STATbits.ACKSTAT == 0){ I2C_Flag = 17; Ack_Flag = 1; }
															else I2C_Flag = 16; 

													I2C1TRN = 0x03;   						// ����� �������� ���� ������
													while( I2C1STATbits.TRSTAT == 1 ){}
													if ( I2C1STATbits.ACKSTAT == 0){ I2C_Flag = 18; Ack_Flag++; }
												
													a++; a++; a++;

													I2C1TRN = 5;   					// �������� ���� - ���� ������
													while( I2C1STATbits.TRSTAT == 1 ){}
													if ( I2C1STATbits.ACKSTAT == 0){ I2C_Flag = 18; Ack_Flag++; }												
													a++; a++; a++;

													I2C1TRN = Day_Bcd;   				// �������� ���� - �����
													while( I2C1STATbits.TRSTAT == 1 ){}
													if ( I2C1STATbits.ACKSTAT == 0){ I2C_Flag = 18; Ack_Flag++; }												
													a++; a++; a++;

													I2C1TRN = Month_Bcd;   				// �������� ���� - �����
													while( I2C1STATbits.TRSTAT == 1 ){}
													if ( I2C1STATbits.ACKSTAT == 0){ I2C_Flag = 18; Ack_Flag++; }												
													a++; a++; a++;

													I2C1TRN = Year_Bcd;   				// �������� ���� - ���
													while( I2C1STATbits.TRSTAT == 1 ){}
													if ( I2C1STATbits.ACKSTAT == 0){ I2C_Flag = 18; Ack_Flag++; }												
													a++; a++; a++;

													I2C1CONbits.PEN = 1; // Stop Receive



}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

 void Scan_Adc_Channels(){
						  // ����� 2

						  Data_To_Send = 25 << 27;		
						  PORTGbits.RG9 = 0;
						  SpiChnPutS( SPI_CHANNEL2 , &Data_To_Send, 1 );																	
							 while( SPI2STATbits.SPIRBF!=1 ){}												
							  PORTGbits.RG9 = 1;									  
									
						  Adc_Data = SpiChnReadC(SPI_CHANNEL2);	
						  Adc_Result = ( Adc_Data & 0x01FFE000 )>>13;
						  Adc_Channel2 = Adc_Result; 	// (unsigned int)( (Adc_Result / 4095.0)*100 );  
								
						  // ����� 1
	
					      Data_To_Send = 24 << 27;		
						  PORTGbits.RG9 = 0;
						  SpiChnPutS( SPI_CHANNEL2 , &Data_To_Send, 1 );																	
							 while( SPI2STATbits.SPIRBF!=1 ){}												
							  PORTGbits.RG9 = 1;									  
									
						  Adc_Data = SpiChnReadC(SPI_CHANNEL2);	
						  Adc_Result = ( Adc_Data & 0x01FFE000 )>>13;
						  Adc_Channel1 = Adc_Result;   // (unsigned int)( (Adc_Result / 4095.0)*100 ); 

						  // ����� 3
	
					      Data_To_Send = 26 << 27;		
						  PORTGbits.RG9 = 0;
						  SpiChnPutS( SPI_CHANNEL2 , &Data_To_Send, 1 );																	
							 while( SPI2STATbits.SPIRBF!=1 ){}												
							  PORTGbits.RG9 = 1;									  
									
						  Adc_Data = SpiChnReadC(SPI_CHANNEL2);	
						  Adc_Result = ( Adc_Data & 0x01FFE000 )>>13;
						  Adc_Channel3 = Adc_Result;    // (unsigned int)( (Adc_Result / 4095.0)*100 ); 

						  // ����� 4
	
					      Data_To_Send = 27 << 27;		
						  PORTGbits.RG9 = 0;
						  SpiChnPutS( SPI_CHANNEL2 , &Data_To_Send, 1 );																	
							 while( SPI2STATbits.SPIRBF!=1 ){}												
							  PORTGbits.RG9 = 1;									  
									
						  Adc_Data = SpiChnReadC(SPI_CHANNEL2);	
						  Adc_Result = ( Adc_Data & 0x01FFE000 )>>13;
						  Adc_Channel4 = Adc_Result;    // (unsigned int)( (Adc_Result / 4095.0)*100 ); 


				
 }

// ---------------------------------------------------------------------------------------------------------------------------------------------------

void Window3(){

			 Window3_Active = 1;
		
			 Send_Lcd_Command(0x01);
			 Set_Cursor(1,3);
			 Send_Lcd_String( "Frequency Range" );

			  //Milisek_Delay(500);
			  
    		 if( Frequency_Range == 1 ){
										Set_Cursor(2,5);
										Send_Lcd_String("100Hz - 1kHz");
										}

			 if( Frequency_Range == 2 ){
										Set_Cursor(2,5);
								        Send_Lcd_String("1kHz - 10kHz");
								       }

			 if( Frequency_Range == 3 ){
										Set_Cursor(2,4);
										Send_Lcd_String("10kHz - 100kHz");
									   }

			 Set_Cursor(3,6);
			 Send_Lcd_String("# - Change");

			 Set_Cursor(4,6);
			 Send_Lcd_String("Step= 1 uS");

								
			 Flags.Key_Scan_Flag = 0; Key_Scan_Timer = 0;
			  Flags.Block_Key_Scan_Flag = 1; Block_Key_Scan_Timer = 0;
		
			   Pressed_Key == ' ';

 while(1){

	if( (Flags.Key_Scan_Flag == 1 ) && ( Flags.Block_Key_Scan_Flag != 1) ){         // ������������ ����������

							 Pressed_Key == ' ';

							 Scan_Code = Key_Scan();

								if( Scan_Code != 7 ){ 
													  Display_Light_Timer = 0;
													   Display_Light_On = 1;     // ����� ������� ������� �������� ��������� ������� �� 30 ������	

									        Pressed_Key = Key_Decode( Scan_Code );

										    Flags.Block_Key_Scan_Flag = 1; Block_Key_Scan_Timer = 0;
						
													 }	// end of 	if( Scan_Code != 7 )

											Flags.Key_Scan_Flag = 0; Key_Scan_Timer = 0;

																			}				// end of  if( Flags.Key_Scan_Flag == 1 )

				if( Pressed_Key == '*' ) break;

				if( (Pressed_Key == '#') && ( PWM_On == 0) ){
										 Frequency_Range++;
										 if( Frequency_Range > 3 ) Frequency_Range = 1;
										
    									 if( Frequency_Range == 1 ){
																	Set_Cursor(2,1);
			  														Send_Lcd_String("                   ");
																	Set_Cursor(2,5);
			  														Send_Lcd_String("100Hz - 1kHz");
																	Freq1_Min = 100; Freq1_Max = 1000;
																	Frequency = Freq1_Min;
																	Recalculate_Pwm_Frequency(Frequency);

																	PWM_Duty_Cycle = (int)(Duty_Cycle * 5);
																	OC2RS =  PWM_Duty_Cycle;			   // OC2RS - ������� ��� PWM_Duty_Cycle
																	}

										 if( Frequency_Range == 2 ){
																	Set_Cursor(2,1);
			  														Send_Lcd_String("                   ");
																	Set_Cursor(2,5);
			  														Send_Lcd_String("1kHz - 10kHz");
																	Freq1_Min = 1000; Freq1_Max = 10000;
																	Frequency = Freq1_Min;
																	Recalculate_Pwm_Frequency(Frequency);

																	PWM_Duty_Cycle = (int)(Duty_Cycle * 10);
																	OC2RS =  PWM_Duty_Cycle;			   // OC2RS - ������� ��� PWM_Duty_Cycle
																	}

										 if( Frequency_Range == 3 ){
																	Set_Cursor(2,1);
			  														Send_Lcd_String("                   ");
																	Set_Cursor(2,4);
			  														Send_Lcd_String("10kHz - 100kHz");
																	Freq1_Min = 10000; Freq1_Max = 100000;
																	Frequency = Freq1_Min;
																	Recalculate_Pwm_Frequency(Frequency);

																	PWM_Duty_Cycle = (int)(Duty_Cycle * 80);
																	OC2RS =  PWM_Duty_Cycle;			   // OC2RS - ������� ��� PWM_Duty_Cycle
																	}									
										}
				 Pressed_Key = ' ';						


			}	//  end of while								


	    First_Screen_Timer = 3;
		 Flags.First_Screen_Flag  = 0; 

	  Window3_Active = 0;
}

	  
// ----------------------------------------------------------------------------------------------------------------------------------------------