/*
???? ?????? ?????????? ??? ???????? ?????????? ?? ??????? 7-?????????? ??????? ? ???????
???????????????? PIC18F4520
PORTD ??????????? ????????? ? ??????? 7-??????????? ???????, PORTB ??????????? ????????? ?
?????????? ULN2003, ??????? ????????? ???????? ????????????? ??????????
??? ?????? ????????????? ??????? ?????????? ????? LCD ??????? ? ?????
 */
#include <p18f4520.h>
#include <delays.h>
#pragma config OSC = HSPLL
#pragma config PWRT = OFF
#pragma config BOREN = OFF
#pragma config WDT = OFF
#pragma config MCLRE = ON
#pragma config PBADEN = OFF
#pragma config LVP = OFF
#define LED0 PORTDbits.RD0
#define LED1 PORTDbits.RD1
#define LED2 PORTDbits.RD2
#define LED3 PORTDbits.RD3
#define LED4 PORTDbits.RD4
#define LED5 PORTDbits.RD5
#define LED6 PORTDbits.RD6
#define LED7 PORTDbits.RD7
#define LEDbuf0 PORTBbits.RB4
#define LEDbuf1 PORTBbits.RB5
#define LEDbuf2 PORTBbits.RB6
#define LEDbuf3 PORTBbits.RB7
// Initializtion

int k = 4, i = 0, time1,time2,time3,time4;

/*
 *????????????? ???????????????? ???????(??????)
 */
void Set_segment_display_v_1(int Data_Byte);
void Set_segment_display_v_2(int Number_Led_Buffer, int Data_Byte);
void Set_segment_display_v_3(int Number_Led_Buffer, int Data_Byte);
void set_number(int Data_Number);
void Interrupt_Isr(void);
//void interrupt ISR();
//void interrupt tc_int(void);

/*
 *????????????? ???????????????? ???????(?????d???)
 */
void init(void) {
    CMCON = 0b00000111; // Close Comparator
    TRISA = 0b00010000; // RA4 is for Switch SW1 Input
    TRISB = 0b00000001; // RB0 is for Switch SW2 Input
    PORTB = 0b00001111;
    TRISC = 0b00000000;
    TRISD = 0b00000000;
    TRISE = 0b00001000; // RE3 is for Switch SW3 Input
    ADCON1 = 0b00001111; // Configure Digital Channel
    //    OPTION_REGbits.T0CS = 0;               // Timer increments on instruction clock
    //        INTCONbits.T0IE = 1;               // Enable interrupt on TMR0 overflow
    //        OPTION_REGbits.INTEDG = 0;             // falling edge trigger the interrupt
    //        INTCONbits.INTE = 1;               // enable the external interrupt
    //        INTCONbits. GIE = 1;                // Global interrupt enable
//    RCONbits.IPEN = 1; //  ????????? ?????????? ??????????
//
//    INTCONbits.GIE = 1; // ????????? ?????????? ??????? ??????
//    INTCONbits.GIEH = 1;
//    INTCONbits.GIEL = 1;

        INTCON=0b10001000;
    //    INTCONbits.INT0E = 1; //enable Interrupt 0 (RB0 as interrupt)
    //    INTCON2bits.INTEDG0 = 0; //cause interrupt at falling edge
    //    INTCONbits.INT0F = 0; //reset interrupt flag
    //    ei();// This is like fliping the master switch to enable interrupt

}
//---------------------------------------------------- MAIN------------------

void main(void) {
    init();
    while (1) {
        time1=16000;
        time2=32000;
        time3=48000;
        time4=64000;
        if (i <= time1) PORTB = 0b00001101;
        if (i > time1 && i <= time2) PORTB = 0b00001011;
        if (i > time2 && i <= time3) PORTB = 0b00001011;
        if (i > time3 && i <= time4) PORTB = 0b00000111;
        if (i == time4) i = 0;
        i++;

        //        for(i=0;i<500;i++){};
        //        Delay10KTCYx(100);
        //        Delay10KTCYx(0);
        //        PORTB = 0b00001011;
        //        Delay10KTCYx(100);
        //        Delay10KTCYx(0);
        //        for(i=0;i<500;i++){};
        //        PORTB = 0b00001011;
        //        Delay10KTCYx(100);
        //        Delay10KTCYx(0);
        //        for(i=0;i<500;i++){};
        //        PORTB = 0b00000111;
        //        Delay10KTCYx(100);
        //        Delay10KTCYx(0);
        //        Delay10KTCYx(0);

    }
}

//--------------------------------------------------END OF MAIN---------------

/*void interruptr()                    // ??????? ????????? ??????????
{
 if(INTCON.RBIF){                 // ???? ??????????? ????????? ?? ??????? ????? B, ? ??????
  if(PORTB.F4==1)                // ???? ?????????? ????????? ?? 4 ?????? ????? ?, ??
    {
      PORTB=0b00000111;      // ?? RB0, RB1 ? RB2 ?????? 1
      Delay_ms(1000);            // ?????
      PORTB=~PORTB;            // ??????????? ?????? ????? ?
    }
  INTCON.RBIF = 0;               // ???????? ???? (????????? ??????????)
 }
}*/


#pragma code

#pragma interrupt Interrupt_Isr

void Interrupt_Isr(void) {
    if (INTCONbits.RBIF) { // ???? ??????????? ????????? ?? ??????? ????? B, ? ??????
        if (PORTBbits.RB0 == 0) // ???? ?????????? ????????? ??  ?????? ????? ?, ??
        {
            PORTB = 0b00001110; // ?? RB0, RB1 ? RB2 ?????? 1
            Delay10KTCYx(0); // ?????
            Delay10KTCYx(0); // ?????
            Delay10KTCYx(0); // ?????
            PORTB = ~PORTB; // ??????????? ?????? ????? ?
        }
        INTCONbits.RBIF = 0; // ???????? ???? (????????? ??????????)
    }
    Delay10KTCYx(0); // ?????
} // end of interrupt routine

//void interrupt tc_int(void){
// if(INTCONbits.RBIF){                 // ???? ??????????? ????????? ?? ??????? ????? B, ? ??????
//  if(PORTBbits.RB0==1)                // ???? ?????????? ????????? ?? 4 ?????? ????? ?, ??
//    {
//      PORTB=0b00000111;     // ?? RB0, RB1 ? RB2 ?????? 1
//      Delay10KTCYx(0);            // ?????
//      PORTB=~PORTB;           // ??????????? ?????? ????? ?
//    }
//  INTCONbits.RBIF = 0;               // ???????? ???? (????????? ??????????)
// }

//};

void Set_segment_display_v_3(int Number_Led_Buffer, int Data_Byte) {
    switch (Number_Led_Buffer) {
        case 1:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 1;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
        case 2:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 1;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
        case 3:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 1;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
        case 4:
        {
            LEDbuf0 = 1;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            set_number(Data_Byte);
            Delay100TCYx(1);
            set_number(-1);
            break;
        }
    }
    Delay100TCYx(1);
}

void set_number(int Data_Number) {
    switch (Data_Number) {
        case 1:
        {
            LED0 = 0;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            break;
        }
        case 2:
        {

            LED0 = 1;
            LED1 = 1;
            LED2 = 0;
            LED3 = 1;
            LED4 = 1;
            LED5 = 0;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 3:
        {

            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 0;
            LED5 = 0;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 4:
        {
            LED0 = 0;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 5:
        {
            break;
        }
        case 6:
        {
            break;
        }
        case 7:
        {
            break;
        }
        case 8:
        {
            break;
        }
        case 9:
        {
            break;
        }
        case 0:
        {
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 1;
            LED5 = 1;
            LED6 = 0;
            LED7 = 0;
            break;
        }
        case -1:
        {
            LED0 = 0;
            LED1 = 0;
            LED2 = 0;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            break;
        }
    }
}

void Set_segment_display_v_1(int Data_Byte) {

    switch (Data_Byte) {
        case 1:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 1;
            LED0 = 0;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            break;
        }
        case 2:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 1;
            LEDbuf3 = 0;
            LED0 = 1;
            LED1 = 1;
            LED2 = 0;
            LED3 = 1;
            LED4 = 1;
            LED5 = 0;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 3:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 1;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 0;
            LED5 = 0;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 4:
        {
            LEDbuf0 = 1;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            LED0 = 0;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 5:
        {
            break;
        }
        case 6:
        {
            break;
        }
        case 7:
        {
            break;
        }
        case 8:
        {
            break;
        }
        case 9:
        {
            break;
        }
        case 0:
        {
            break;
        }
    }

}

void Set_segment_display_v_2(int Number_Led_Buffer, int Data_Byte) {
    switch (Number_Led_Buffer) {
        case 1:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 1;
            break;
        }
        case 2:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 0;
            LEDbuf2 = 1;
            LEDbuf3 = 0;
            break;
        }
        case 3:
        {
            LEDbuf0 = 0;
            LEDbuf1 = 1;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            break;
        }
        case 4:
        {
            LEDbuf0 = 1;
            LEDbuf1 = 0;
            LEDbuf2 = 0;
            LEDbuf3 = 0;
            break;
        }
    }
    switch (Data_Byte) {
        case 1:
        {
            LED0 = 0;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 0;
            LED6 = 0;
            LED7 = 0;
            break;
        }
        case 2:
        {

            LED0 = 1;
            LED1 = 1;
            LED2 = 0;
            LED3 = 1;
            LED4 = 1;
            LED5 = 0;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 3:
        {

            LED0 = 1;
            LED1 = 1;
            LED2 = 1;
            LED3 = 1;
            LED4 = 0;
            LED5 = 0;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 4:
        {
            LED0 = 0;
            LED1 = 1;
            LED2 = 1;
            LED3 = 0;
            LED4 = 0;
            LED5 = 1;
            LED6 = 1;
            LED7 = 0;
            break;
        }
        case 5:
        {
            break;
        }
        case 6:
        {
            break;
        }
        case 7:
        {
            break;
        }
        case 8:
        {
            break;
        }
        case 9:
        {
            break;
        }
        case 0:
        {
            break;
        }
    }
    Delay100TCYx(2);


}


