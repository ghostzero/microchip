
/*
���� ������ ���������� ������ ������ UART � ���������������� PIC18F4520
��������� ��� ����� �� �� ��� �������� �����������������: 9600���, 8 ��� ������, 1 ���� ���
*/

#include <p18f4520.h>
#include <sw_i2c.h>
#include <delays.h>
#include <usart.h>
#pragma config OSC = HS
#pragma config PWRT = OFF
#pragma config BOREN = OFF
#pragma config WDT = OFF
#pragma config MCLRE = ON
#pragma config PBADEN = OFF
#pragma config LVP = OFF
// Initializtion
void init(void)
{
CMCON=0b00000111; // Close Comparator
TRISA=0b00010000; // RA4 is for Switch SW1 Input
TRISB=0b00000001; // RB0 is for Switch SW2 Input
TRISC=0b00000000;
TRISD=0b00000000;
TRISE=0b00001000; // RE3 is for Switch SW3 Input ADCON1=0b00001111; // Configure Digital Channel
SPBRG=31; // Baud Rate 9600bps
BAUDCONbits.BRG16=0; // Choose 8-bit Baud Rate Generator
TXSTAbits.BRGH=0; // High Baud Rate
TXSTAbits.SYNC=0; // Asynchronous Mode
RCSTAbits.SPEN=1; // Enable Serial Port
TXSTAbits.TX9=0; // Transmit 8-bit data
TXSTAbits.TXEN=1; // Enable Transmission
}
// Main Programmer
void main( void )
{
union USART USART_Status;
char wr_data[]={"Hello world"};
char rd_data[7];
init(); // Initializtion Program
Delay10KTCYx(200);
putsUSART(wr_data); // Transmit Data
while(!PIR1bits.TXIF)
continue;
}