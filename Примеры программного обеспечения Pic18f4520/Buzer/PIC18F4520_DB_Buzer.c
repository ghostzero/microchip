/*
� ���� ������� ��������, ��� ��������� ������� ������, ������������ � PORTC2 (CCP1) 
������ ��������� � ���������� NPN ����������� ���������� ULN2003
���� ����������� ���������� ULN2003 ����������� ������ PORTC2 (CCP1)
����� ������ PWM � ������ PORTC2 ��������� �� ���� �����������, ������ �������� �������.
*/
#include <p18f4520.h>
#include <delays.h>
#pragma config OSC = HS
#pragma config PWRT = OFF
#pragma config BOREN = OFF
#pragma config WDT = OFF
#pragma config MCLRE = ON
#pragma config PBADEN = OFF
#pragma config LVP = OFF
void init(void);
void PWM(unsigned char i);
// Initializtion
void init(void)
{
CMCON=0b00000111; // Close Comparator
TRISA=0b00010000;
TRISB=0b00000001;
TRISC=0b00000000;
TRISD=0b00000000;
TRISE=0b00001000;
ADCON1=0b00001111; // Configure Digital Channel
}
// Set PWM Mode
void PWM(unsigned char i)
{
CCP1CON=0b00001100;
T2CONbits.TMR2ON = 0;
T2CONbits.T2OUTPS3 = 1;
T2CONbits.T2OUTPS2 = 1;
T2CONbits.T2OUTPS1 = 1;
T2CONbits.T2OUTPS0 = 1;
T2CONbits.T2CKPS1 = 1;
T2CONbits.T2CKPS1 = 1;
PR2 = 255;
TRISCbits.TRISC2=0;
T2CONbits.TMR2ON = 1;
CCPR1L = 25*i;
}
// Main Programmer
void main( void )
{
init(); // Initializtion
while(1)
{
PWM(4); //Buzzer Beep
Delay1KTCYx(255);
PWM(0);
Delay1KTCYx(255);
}
}