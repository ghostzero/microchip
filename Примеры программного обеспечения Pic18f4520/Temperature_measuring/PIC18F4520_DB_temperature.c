/*
���� ������ ���������� ��� ��������� �������� ����������� � ������� ����������� LM75A 
����� I2C ��������� ���������������� PIC18F4520.
� ���� ������� �������� ������ ������ I2C ���������������� PIC18F4520 
��� ���������� ������ I2C �� ������ �������� sw_i2c.h �� hw_i2c.h � �������������� ��� ���������
LM75A ���������� � ������ PORTC3 � PORTC4. 
*/
#include <p18f4520.h>
#include <sw_i2c.h>
#pragma config OSC = HS
#pragma config PWRT = OFF
#pragma config BOREN = OFF
#pragma config WDT = OFF
#pragma config MCLRE = ON
#pragma config PBADEN = OFF
#pragma config LVP = OFF
unsigned char i2c_var;
unsigned int cvalue,fvalue; // Memory Centigrade and Fahrenheit value
unsigned char cent_buf[5],fahr_buf[5]; // Centigrade and Fahrenheit value array
// Initializtion
void init(void)
{
CMCON=0b00000111; // Close Comparator
TRISA=0b00010000;
TRISB=0b00000001;
TRISC=0b00000000;
TRISD=0b00000000;
TRISE=0b00001000;
ADCON1=0b00001111; // Configure Digital Channel
}
void LM75_init(void) // Temperature Sensor Initializtion
{
SWStartI2C();
i2c_var = SWPutcI2C(0x90); // Control Byte
SWAckI2C();
i2c_var = SWPutcI2C(0x01); // Configure Register
SWAckI2C();
i2c_var = SWPutcI2C(0x18); // Configure Byte
SWAckI2C();
SWStopI2C();
}
void LM75_temperature(void)
{
unsigned char tptr[2];
unsigned int temp_H,temp_L;
SWStartI2C();
i2c_var = SWPutcI2C(0x90); // Control Byte
SWAckI2C();
i2c_var = SWPutcI2C(0x00); // Data Address
SWAckI2C();
SWRestartI2C();
i2c_var = SWPutcI2C(0x91); // Control Byte
SWAckI2C();
i2c_var = SWGetsI2C(tptr, 2); // Read Temperature Value
SWStopI2C();
temp_H=tptr[0]; // High Bits
temp_L=tptr[1]; // Low Bits
// Compute Centigrade
cvalue=(temp_H<<8)|temp_L;
cent_buf[0]=' ';
if(cvalue&0x80==1)
{
cvalue=~cvalue+1; //Calculate Base Complement
cent_buf[0]='-';
}
cvalue=cvalue>>5;
cvalue=cvalue * 1.25;
cent_buf[1]=cvalue/100+48;
cent_buf[2]=(cvalue/10)%10+48;
cent_buf[3]='.';
cent_buf[4]=cvalue%10+48;
cent_buf[5]='\0';
// Compute Fahrenheit
fvalue=((cvalue*9)/5)+32;
fahr_buf[0]=' ';
if(fvalue&0x80==1)
{
fvalue=~fvalue+1; //Calculate Base Complement
fahr_buf[0]='-';
}
fahr_buf[1]=fvalue/100+48;
fahr_buf[2]=(fvalue/10)%10+48;
fahr_buf[3]='.';
fahr_buf[4]=fvalue%10+48;
fahr_buf[5]='\0';
}
// Main Programmer
void main( void )
{
init(); // Initialize Control Microchip
LM75_init(); // Temperature Sensor Initializtion
while(1)
{
LM75_temperature(); // Read Temperature Value
}
}