
#include <p18f4520.h>
#include <delays.h>
#pragma config OSC = HSPLL // High-Speed Crystal/Resonator
//with PLL enabled
#pragma config PWRT = OFF
#pragma config BOREN = OFF
#pragma config WDT = OFF
#pragma config MCLRE = ON
#pragma config PBADEN = OFF
#pragma config LVP = OFF
// Initializtion
void init(void)
{
CMCON=0b00000111; // Close Comparator
TRISA=0b00010000;
TRISB=0b00000001;
TRISC=0b00000000;
TRISD=0b00000000;
TRISE=0b00001000;
ADCON1=0b00001111; // Configure Digital Channel
}
// Main Program
void main( void )
{
init(); // Initialize Microchip
while(1)
{
PORTBbits.RB1=0;
Delay1KTCYx(255);
PORTBbits.RB1=1;
Delay1KTCYx(255);
PORTBbits.RB2=0;
Delay1KTCYx(255);
PORTBbits.RB2=1;
Delay1KTCYx(255);
PORTBbits.RB3=0;
Delay1KTCYx(255);
PORTBbits.RB3=1;
Delay1KTCYx(255);
}
}