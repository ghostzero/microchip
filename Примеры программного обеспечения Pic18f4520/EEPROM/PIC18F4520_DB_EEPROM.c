/*
���� ������ ���������� ��� ��������� � ���������� ������ � ������ 24�04 � ������� ���������������� PIC18F4520
������ 24�04 ���������� � ������ PORTC3 � PORTC4 ���������������� PIC18F4520
*/
#include <p18f4520.h>
#include <sw_i2c.h>
#include <delays.h>
#pragma config OSC = HS
#pragma config PWRT = OFF
#pragma config BOREN = OFF
#pragma config WDT = OFF
#pragma config MCLRE = ON
#pragma config PBADEN = OFF
#pragma config LVP = OFF
unsigned char i2c_var;
unsigned char wr_data[]={0x0a,0x0b,0x0c,0x0d};
unsigned char rd_data[4];
// Initializtion
void init(void)
{
CMCON=0b00000111; // Close Comparator
TRISA=0b00010000; // RA4 is for Switch SW1 Input
TRISB=0b00000001; // RB0 is for Switch SW2 Input
TRISC=0b00000000;
TRISD=0b00000000;
TRISE=0b00001000; // RE3 is for Switch SW3 Input
ADCON1=0b00001111; // Configure Digital Channel
}
// Write data
void byte_write(unsigned char adr,unsigned char data)
{
SWStartI2C();
i2c_var = SWPutcI2C(0xA0); // Control Byte
SWAckI2C();
i2c_var = SWPutcI2C(adr); // Word address
SWAckI2C();
i2c_var = SWPutcI2C(data); // Write Data
SWAckI2C();
SWStopI2C();
}
// Read data
void byte_read(unsigned char adr)
{
SWStartI2C();
i2c_var = SWPutcI2C( 0xA0 ); // Control Byte
SWAckI2C();
i2c_var = SWPutcI2C(adr); // Word Address
SWAckI2C();
SWRestartI2C();
i2c_var = SWPutcI2C( 0xA1 ); // Control Byte
SWAckI2C();
i2c_var = SWGetcI2C(); // Get Data
SWStopI2C();
}
// Write string
void page_write(unsigned char adr,unsigned char wdata[])
{
SWStartI2C();
i2c_var = SWPutcI2C(0xA0); // Control Byte
SWAckI2C();
i2c_var = SWPutcI2C(adr); // Word Address
SWAckI2C();
i2c_var = SWPutsI2C(wdata); // Get Data
SWStopI2C();
}
// Read string
void sequential_read(unsigned char adr,unsigned char rdata[],unsigned char len)
{
SWStartI2C();
i2c_var = SWPutcI2C( 0xA0 ); // Control Byte
SWAckI2C();
i2c_var = SWPutcI2C(adr); // Word Address
SWAckI2C();
SWRestartI2C();
i2c_var = SWPutcI2C( 0xA1 ); // Control Byte
SWAckI2C();
i2c_var = SWGetsI2C(rdata,len); // Get Data
SWStopI2C();
}
// Inquiries confirmed
void ack_poll( void )
{
SWStartI2C();
i2c_var = SWPutcI2C( 0xA0 ); // Control Byte
while( SWAckI2C() )
{
SWRestartI2C();
i2c_var = SWPutcI2C(0xA0); // Write Data
}
SWStopI2C();
}
// Main Programmer
void main( void )
{
init(); // Initialize Control Microchip
while(1)
{
ack_poll();
page_write(0x02,wr_data);
ack_poll();
Nop();
sequential_read(0x02,rd_data,4);
Nop();
}
}
